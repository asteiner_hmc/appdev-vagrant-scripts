
# Monthly Local Liferay Vagrant Refreshes

## Prerequisites

### To Run
* Packer
* Vagrant
* Virtual Box
* Bash v4 or higher
	* OSX ships with v3
	* Win: cygwin works. git-bash *might* work.
* pv
* GNU sed
* curl

### To Develop
* Everything in Run
* argbash (Not available for Windows.  I used a Linux Vm with a small footprint.
* shellcheck (Win: Available from Chocolatey)

## Request Database Exports
1. Go to ServiceNow, and under **Self-Service**, click **Create a Generic Request**.
1. Enter the following values for the given fields:
    * Callback and Location Info
        * Same as business phone: **Checked**
    * Request Info
        * Into which category does your request best fit?: **Infrastructure**
        * Brief description of your request:  
        
            ```
            Provide exports of the INFONET71, INFONET73, EWP73, and MED schemas (<month> <year>)
            ```

        * Include more details, that would support this request (Do not include PHI in this field, it is not encrypted): 

            ```
            Provide exported dump files for the following schemas.  Intention is to load this data into Local development Oracle DB instances.
            These exports are created from the Export Jobs that end with "_PSHDEV", as they push the dump files to the pshdevtools server where they are accessible to the AppDev Team.
            
            Please schedule these exports for <DATE: Friday, August 2nd> at <TIME: 8pm-12am>.


            INFONET (7.1)
              Host: psh01cen1appdb1.pennstatehealth.net
              SID: INTWEB
              Schema: INFONET71
              
            INFONET (7.3)
              Host: psh01cen1appdb1.pennstatehealth.net
              SID: INTWEB
              Schema: INFONET73

            EWP
              Host: psh01cen1appdb2.pennstatehealth.net
              SID: EXTWEB
              Schema: EWP73

            MED
              Host: psh01cen1appdb2.pennstatehealth.net
              SID: EXTWEB
              Schema: MED

            Message me if you have any questions.
            ```

1. Click **Order Now** (located at the top-right corner of the page).

1. Click on the RITM in the list.

1. Enter the following values for the given fields:
    * Assignment Group: **Database Admin**

    * Requested By Information
        * Request Date: <DATE: Friday, August 2nd> <TIME: 8pm-12am>

1. Click **Save**.

## Generate Steps To Run

1. Wait for the exports to become available.
1. Once you have the "DB exported" Emails, update `workflow_initiative/vars-global.sh` with the appropriate values.
1. Update `package-vagrants/*_Vagrantfile` with the new ORCL VM Version Date.
1. Run `bash workflow_initiative/01_wf_combined_to_xml.sh`.
1. A HTML File with the commands will be generated.  The *file:///* URL will be in *01_wf_combined_to_xml.sh*'s output. Open the HTML File and run the commands.



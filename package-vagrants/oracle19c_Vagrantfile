# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.require_version ">= 2.2.19"

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
Vagrant.configure("2") do |config|

  # The most common configuration options are documented and commented below.
  # For a complete reference, please see the online documentation at
  # https://docs.vagrantup.com.

  config.vm.box = "oracle19c"
  config.vm.hostname = "oracle19c-local"

  ## Port Numbers are based on a starting number.  This is to allow multiple Portals to run side-by-side, while keeping the
  ## actual port numbers predictable.
  ## liferay62 = 00, ewp62 = 01, liferay71 = 02, infonet71 = 03, ewp73 = 04, liferay73 = 05, infonet73 = 06, oracle19c = 99
  ## One exception (for Port Numbers at least) are the Oracle VMs, that use 1521 for DB service port number.

  config.vm.network "forwarded_port", guest: 1521,  host: 1521,  id: "orcl"             # Oracle
  config.vm.network "forwarded_port", guest: 22,    host: 59901, id: "ssh"              # SSH

  # ATS - In our other Vagrant boxes, both network interfaces are added to the routing table (ip route) as default.
  # In this vagrant box, however, the last added interface is being set as the default.
  # This results in this box not being able to make connections to hosts outside of OSX.
  # I couldn't figure out why, and for now, just have to live with it.
  # This may turn out to be a non-issue;  in normal usage, the Oracle VM would not have to communicate with external services.
  # ATS 10/11/2022 - Seems to work now?

  config.vm.network "private_network", adapter: 2, ip: "192.168.60.199", netmask:"255.255.255.0"

  config.vm.post_up_message =
     <<~HEREDOC
     This Vagrant Box is a repackage of Oracle's 19c Vagrant (19.3), with proper Guest Additions and
     extra disk space to support larger database imports.

     Oracle's documentation on this box can be found at: [https://github.com/oracle/vagrant-projects/tree/main/OracleDatabase/19.3.0#readme]
     That information is accurate except for the following:
         PDB SID:  ORCL
         Password: The name of the vendor, all lowercase.

     Important Info:
         All Oracle tools (SQLcl, Data Pump, etc) are available as the 'oracle' user. After SSH-ing, run the following:
             sudo su - oracle

         When creating new Schemas, create them in /BigTablespace1, which can go to 300GB.

     PORTS/SERVICES:
         ORCL -> 1521
     HEREDOC

  config.vm.provider "virtualbox" do |vb|
    # Customize the amount of memory on the VM:
    vb.memory = "3072"
    vb.cpus = 2

    # Change the VM's execution cap to 96%, so that the VM does not take over the Host's CPU.
    vb.customize ["modifyvm", :id, "--cpuexecutioncap", "96" ]
  end

end

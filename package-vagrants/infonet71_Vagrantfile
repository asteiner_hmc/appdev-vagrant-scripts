# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.require_version ">= 2.2.19"

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
Vagrant.configure("2") do |config|

  # The most common configuration options are documented and commented below.
  # For a complete reference, please see the online documentation at
  # https://docs.vagrantup.com.

  config.vm.box = "infonet71"
  config.vm.hostname = "infonet71-local"

  ## Port Numbers are based on a starting number.  This is to allow multiple Portals to run side-by-side, while keeping the
  ## actual port numbers predictable.
  ## liferay62 = 00, ewp62 = 01, liferay71 = 02, infonet71 = 03, ewp73 = 04, liferay73 = 05, infonet73 = 06, oracle19c = 99

  config.vm.network "forwarded_port", guest: 443,   host: 50300, id: "https"            # Main HTTPS
  config.vm.network "forwarded_port", guest: 22,    host: 50301, id: "ssh"              # SSH
  config.vm.network "forwarded_port", guest: 8025,  host: 50302, id: "mailhog"          # MailHog Web UI/REST API
  config.vm.network "forwarded_port", guest: 7007,  host: 50303, id: "jpda"             # JPDA (Remote Debugging)
  config.vm.network "forwarded_port", guest: 50304, host: 50304, id: "jmx"              # JMX (Monitoring with VisualVM). Ports have to match, due to how JMX works.
  config.vm.network "forwarded_port", guest: 9200,  host: 50305, id: "elasticsearch"    # ElasticSearch

  # Configure second NIC to Host-Only network. Used for communicating with other VMs, like Oracle VM.
  config.vm.network "private_network", adapter: 2, ip: "192.168.60.103", netmask: "255.255.255.0"

  #config.vm.synced_folder "/data/LDS3/vagrant_infonet71_deploy", "/apps/liferay/deploy", owner:"liferay", group:"liferay", create: true

  config.vm.post_up_message =
    "This box requires an AppDev Orcl Web Prod VM with INFONET71 (v20240411) loaded and running.\n"\
    "\n"\
    "\n"\
    "SITES:\n"\
    "    HMC Infonet v2 -> https://infonet-local:50300\n"\
    "    PSH Infonet v2 -> https://psh-infonet-local:50300\n"\
    "\n"\
    "PORTS/SERVICES:\n"\
    "    HTTPS            -> 50300\n"\
    "    SSH              -> 50301\n"\
    "    JPDA (Debugging) -> 50303\n"\
    "    JMX (VisualVM)   -> 50304\n"\
    "    ElasticSearch    -> 50305\n"\
    "\n"\
    "    See emails sent from the VM: http://localhost:50302\n"\
    "\n"\
    "IMPORTANT: Liferay starts automatically with the OS.\n"\
    "    To stop: sudo systemctl stop liferay\n"\
    "    To restart: sudo systemctl restart liferay\n"\
    "    To stop Liferay from starting automatically: sudo systemctl disable liferay\n"\
    "\n"\
    "TO VIEW THE CONSOLE:\n"\
    "    tail -f /apps/liferay/tomcat/logs/catalina.out"

  # Provider-specific configuration so you can fine-tune various
  # backing providers for Vagrant. These expose provider-specific options.
  config.vm.provider "virtualbox" do |vb|
    # Customize the amount of memory on the VM:
    vb.memory = "4096"
    vb.cpus = 2

    # Change the VM's execution cap to 96%, so that the VM does not take over the Host's CPU.
    vb.customize ["modifyvm", :id, "--cpuexecutioncap", "96" ]
  end

end

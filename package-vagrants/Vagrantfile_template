# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.require_version ">= 2.2.19"

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
Vagrant.configure("2") do |config|

  # The most common configuration options are documented and commented below.
  # For a complete reference, please see the online documentation at
  # https://docs.vagrantup.com.

  config.vm.box = "<BOX_NAME>"
  config.vm.hostname = "<BOX_NAME>-local"

  ## Port Numbers are based on a starting number.  This is to allow multiple Portals to run side-by-side, while keeping the
  ## actual port numbers predictable.
  ## liferay62 = 00, ewp62 = 01, liferay71 = 02, infonet71 = 03, ewp73 = 04, liferay73 = 05, infonet73 = 06, oracle19c = 99

  config.vm.network "forwarded_port", guest: 443,   host: 5<NUM>00, id: "https"            # Main HTTPS
  config.vm.network "forwarded_port", guest: 22,    host: 5<NUM>01, id: "ssh"              # SSH
  config.vm.network "forwarded_port", guest: 8025,  host: 5<NUM>02, id: "mailhog"          # MailHog Web UI/REST API
  config.vm.network "forwarded_port", guest: 7007,  host: 5<NUM>03, id: "jpda"             # JPDA (Remote Debugging)
  config.vm.network "forwarded_port", guest: 5<NUM>04, host: 5<NUM>04, id: "jmx"              # JMX (Monitoring with VisualVM). Ports have to match, due to how JMX works.
  config.vm.network "forwarded_port", guest: 9200,  host: 5<NUM>05, id: "elasticsearch"    # ElasticSearch
  config.vm.network "forwarded_port", guest: 3306,  host: 5<NUM>06, id: "mysql"            # MySQL

  # Configure second NIC to Host-Only network "vboxnet0". Used for communicating with other VMs, like Oracle VM.
  config.vm.network "private_network", adapter: 2, ip: "192.168.60.1<NUM>", netmask: "255.255.255.0"

  #config.vm.synced_folder "/data/LDS3/vagrant_<CONFIG_THIS_ENV>_deploy", "/apps/liferay/deploy", owner:"liferay", group:"liferay", create: true

  config.vm.post_up_message =
    "This box requires an AppDev Orcl Web Prod VM with <SCHEMA_NAME> (v<DATA_OVA_VERSION>) loaded and running.\n"\
    "\n"\
    "\n"\
    "SITES:\n"\
    "    Site 1 -> https://localhost:5<NUM>00\n"\
    "\n"\
    "PORTS/SERVICES:\n"\
    "    HTTPS            -> 5<NUM>00\n"\
    "    SSH              -> 5<NUM>01\n"\
    "    JPDA (Debugging) -> 5<NUM>03\n"\
    "    JMX (VisualVM)   -> 5<NUM>04\n"\
    "    ElasticSearch    -> 5<NUM>05\n"\
    "    MySQL            -> 5<NUM>06\n"\
    "\n"\
    "    See emails sent from the VM: http://localhost:5<NUM>02\n"\
    "\n"\
    "IMPORTANT: Liferay starts automatically with the OS.\n"\
    "    To stop: sudo systemctl stop liferay\n"\
    "    To restart: sudo systemctl restart liferay\n"\
    "    To stop Liferay from starting automatically: sudo systemctl disable liferay\n"\
    "\n"\
    "TO VIEW THE CONSOLE:\n"\
    "    sudo -u liferay tail -f /apps/liferay/tomcat/logs/catalina.out"

  # Provider-specific configuration so you can fine-tune various
  # backing providers for Vagrant. These expose provider-specific options.
  config.vm.provider "virtualbox" do |vb|
    # Customize the amount of memory on the VM:
    vb.memory = "4096"
    vb.cpus = 2

    # Change the VM's execution cap to 96%, so that the VM does not take over the Host's CPU.
    vb.customize ["modifyvm", :id, "--cpuexecutioncap", "96" ]
  end

end

#!/usr/bin/env bash
###################################################################
#
#    Downloads and installs VirtualBox Guest Additions
#    This is meant to be run from the guest machine.
#
#    Parameters
#        1    Version. Optional.  If not given, then the script tries to figure it out from the system.
#
###################################################################


VERSION="${1:-}"

if [[ -z "${VERSION}" ]]; then
    echo 'Version not specified.  Querying OS.'
    VERSION="$(dmidecode | grep -i vboxver | grep -E -o '[[:digit:]\.]+' | tail -n 1)"
    echo "OS reports that we are in VirtualBox ${VERSION}."
fi
if [[ -z "${VERSION}" ]]; then
    echo 'No version given, and could not determine version from OS. Exiting.'
    exit 1
fi

# Check if the version to be installed is already installed.
loaded_vbga_version="$(modinfo vboxguest -F version | sed -n -E 's/(^[[:digit:]\.]+).*/\1/p')"
if [[ "${VERSION}" == "${loaded_vbga_version}" ]]; then
    echo "Guest Additions ${loaded_vbga_version} are already loaded"
    exit 0
fi


tmp_dir="$(mktemp --directory)"

curl -o "${tmp_dir}/VBGA.iso" "https://download.virtualbox.org/virtualbox/${VERSION}/VBoxGuestAdditions_${VERSION}.iso"

function finish {
    umount --force /mnt/vbga_iso
    rm -rf /mnt/vbga_iso
    rm -rf "${tmp_dir}"
}
trap finish EXIT


mkdir /mnt/vbga_iso
mount -t iso9660 -r -o loop "${tmp_dir}/VBGA.iso" /mnt/vbga_iso/

# VBoxLinuxAdditions.run does not return 0 upon success.
# See: https://stackoverflow.com/questions/25434139/vboxlinuxadditions-run-never-exits-with-0

set +e
/mnt/vbga_iso/VBoxLinuxAdditions.run  ; echo "Exit Code: $?"
set -e
loaded_vbga_version="$(modinfo vboxguest -F version | sed -n -E 's/(^[[:digit:]\.]+).*/\1/p')"
echo "Guest Additions ${loaded_vbga_version} is running"

# Exit with 1 if the detected GA version does not match what we intended to install
[[ "${VERSION}" == "${loaded_vbga_version}" ]] || exit 1

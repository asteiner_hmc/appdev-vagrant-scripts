#!/usr/bin/env bash

###################################################################
#
#    Tweaks to the liferay73 Vagrant to reduce issues, and better
#    match the Data Center servers.
#
###################################################################

# Exit immediately on non-zero status
set -e -E -u -o pipefail


# Clean out "data" folder except for the license
find /apps/liferay/data/ -mindepth 1 -maxdepth 1 -type d ! -name 'license' -exec rm -r "{}" \;

exit 0
#!/usr/bin/env bash

###################################################################
#
# elasticsearch.sh
#
#   Installs an instance of ElasticSearch, and adds the given Liferay PLugin to the deploy folder.
#	Liferay's portal properties will still need to be configured to utilize ELasticSearch.
#
#   Environment variables expected to be set:
#		ES_RPM_URL 				(ex: https://download.elastic.co/elasticsearch/elasticsearch/elasticsearch-6.5.4.noarch.rpm)
#
#
###################################################################

# Exit immediately on non-zero status
set -e -E -u -o pipefail

# Uncomment to see commands that fail.
# trap 'echo "Error at ${BASH_SOURCE[0]}:$LINENO. Command returned $? : $BASH_COMMAND" ; exit 255' ERR


echo 'Installing ElasticSearch'
yum install "$ES_RPM_URL" --assumeyes

es_yml_path="$(rpm -ql elasticsearch | grep --color=never 'elasticsearch.yml$')"

if [[ ! -f "${es_yml_path}" ]]; then
    echo "ElasticSearch YML file not found at ${es_yml_path}. Exiting."
    exit 1
fi    

# Comment out existing "node.name" and "http.host" settings, if any
sed -i -r --follow-symlinks "s/^\\s*node\.name:.*/# &/" "$es_yml_path"
sed -i -r --follow-symlinks "s/^\\s*http\.host:.*/# &/" "$es_yml_path"
# Set new ones
tee -a "$es_yml_path" <<- EOL_TEE > /dev/null
	node.name: "ElasticSearch"
	http.host: 0.0.0.0
EOL_TEE

# So far, this has always been the installation folder
es_home='/usr/share/elasticsearch'
#...but test that our es_home has the main executable where it's expected
if [[ ! -f "${es_home}/bin/elasticsearch" ]]; then
    echo "${es_home} doesn't seem to be where ElasticSearch is installed.  Please update the script with accurate logic and try again."
    exit 2
fi

es_tmpdir="${es_home}/tmp"
mkdir -p "${es_tmpdir}"
chown elasticsearch:elasticsearch "${es_tmpdir}"

tee -a /etc/sysconfig/elasticsearch <<- EOL_TEE > /dev/null
	# Temp Folder
	ES_TMPDIR=${es_tmpdir}
EOL_TEE

systemctl daemon-reload

systemctl start elasticsearch.service

# Install Plugins needed by Liferay 7
es_plugin_bin="${es_home}/bin/elasticsearch-plugin" # "$(rpm -ql elasticsearch | grep 'bin/elasticsearch-plugin$')"
$es_plugin_bin install analysis-icu
$es_plugin_bin install analysis-kuromoji
$es_plugin_bin install analysis-smartcn
$es_plugin_bin install analysis-stempel

systemctl stop elasticsearch.service

systemctl enable elasticsearch.service

#### Allow outside access to ElasticSearch
firewall-cmd --quiet --add-port=9200/tcp
firewall-cmd --quiet --add-port=9200/tcp --permanent



exit 0

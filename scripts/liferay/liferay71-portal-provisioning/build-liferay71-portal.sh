#!/usr/bin/env bash

###################################################################
#
#    Spins up a new Liferay 71 Vagrant and configures it to run one
#    of our Portals (Infonet, Public Portal).
#
#    Parameters
#        1    vagrant_dir       Directory to use for Vagrant Box
#        2    vm_memory         VM Memory.  Value is in MB (2044, 5120, etc)
#        3    jvm_max_mem       (2g, 3g, 4g, etc)
#        4    box_vagrant_file  Vagrantfile to use for the Box's initial provisioning.
#                               This should be the same Vagrantfile that is exported
#                               with the Box during publishing.
#
#    Environment variables expected to be set:
#        BOX_BASE_VAGRANTFILE
#        CONFIG_LIFERAY_ARTIFACT_BASE
#        BOX_HOSTS_ENTRIES
#        CONFIG_THIS_ENV
#        CONFIG_JMX_PORT
#        CONFIG_REPOS
#        MAILHOG_BINARY_URL
#        ES_RPM_URL
#        ORACLE_CONNECTOR_JAR_URL
#        ORACLE_VM_HOST
#        ORACLE_VM_IP
#        CONFIG_ORACLE_SCHEMA_USER
#        CONFIG_ORACLE_SCHEMA_PASS
#
###################################################################

set -e -E -u -o pipefail

# Uncomment to see commands that fail.
# trap 'echo "Error at ${BASH_SOURCE[0]}:$LINENO. Command returned $? : $BASH_COMMAND" ; exit 255' ERR

case "$(uname -sr)" in
    Darwin*)
        # Mac OS X
        #For OSX, use coreutils if installed
        PATH="/usr/local/opt/coreutils/libexec/gnubin:$PATH"
        ;;

    CYGWIN*|MINGW*|MINGW32*|MSYS*)
        # CYGWIN on Windows
        ;;

    # Future Use
    Linux*Microsoft*)
        ;&
    Linux*)
        ;&
    *)
        # Other
        echo "OS $(uname -sr) is not supported by this script"
        exit 255
        ;;
esac


# Get Directory that the script lives in
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

vagrant_dir="${1:-}"

if [[ -z "${vagrant_dir}" ]]; then
    echo 'Please pass in the folder that will be used for the Vagrant box'
    exit 1
fi

vm_memory="$2"

jvm_max_mem="$3"

box_vagrant_file="$4"

# Change vagrant_dir to the full path so that the "finally" function can clean up even if we change to a different directory.
vagrant_dir="$(realpath "${vagrant_dir}")"

if [[ -d "${vagrant_dir}" ]]; then
    cd "${vagrant_dir}"

    echo 'Destroying existing Vagrant box' # if there was one
    vagrant destroy --force || :

    rm -rf --preserve-root ./*
else
    mkdir -p "${vagrant_dir}"
    cd "${vagrant_dir}"
fi

# Spot check that Apache httpd is running on Host
#if ! curl -sf http://localhost:8080/downloads > /dev/null; then
#    echo $'Apache isn\'t running on the Host machine.'
#    exit 1
#fi

## Current Directory is the Vagrant folder

echo 'Creating Vagrantfile'

## Configure the box by extending the Vagrantfile that will be exported
## with the final VM, and changing a few points needed for the
## provisioning to work.
cp "${box_vagrant_file}" ./Vagrantfile-base

tee ./Vagrantfile <<- EOL_VAGRANTFILE > /dev/null
	include_vagrantfile = File.expand_path("../Vagrantfile-base", __FILE__)
	load include_vagrantfile if File.exist?(include_vagrantfile)

	# Override the "config.vm.box" setting so that Vagrant will construct
	# the VM from the Base liferay71 Box
	Vagrant.configure("2") do |config|
	  config.vm.box = "liferay71"
	  config.vm.box_check_update = false

	  # Use more memory during provisioning to make things easier
	  config.vm.provider "virtualbox" do |vb|
	    vb.memory = "${vm_memory}"
	  end
	end
EOL_VAGRANTFILE


vagrant up


# Copy scripts and assets into shared folder
scripts_dir="$(realpath "${SCRIPT_DIR}"/../../ )"
cp -r "${SCRIPT_DIR}"/guest .
cp -r "${scripts_dir}"/vbga.sh ./guest/scripts
cp -r "${scripts_dir}"/liferay/configure-tomcat-jvm-memory.sh ./guest/scripts
cp -r "${scripts_dir}"/liferay/configure-to-oraclevm.sh ./guest/scripts


# Save vagrant ssh config info for easier scripting
echo "Getting SSH Config for Vagrant"
vagrant ssh-config > ./vagrant-ssh-config

function finish {
  rm -rf "${vagrant_dir}"/vagrant-ssh-config
  rm -rf "${vagrant_dir}"/guest
}
trap finish EXIT


# Extract the Host key Ex: Host: oracle-19c-vagrant
ssh_host_key="$(perl -ne 'exit if /^Host (\S*)\R?$/ && print "$1\n"' ./vagrant-ssh-config)"


echo 'Installing / Updating VirtualBox Guest Additions'
vbox_version="$(vboxmanage --version | perl -ne '/^(.*?)r/ && print "$1\n"')"
ssh -F vagrant-ssh-config "${ssh_host_key}" bash <<- EOL_SSH
	set -e  # If the following script fails, then return back to main script
	sudo bash /vagrant/guest/scripts/vbga.sh "${vbox_version}"
EOL_SSH

vagrant reload


# Stop-gap: Load configs/environment variables for this into a shell script until I can
# convert their usage to script arguments

#tee ./script.sh <<- EOL_SSH
ssh -F vagrant-ssh-config "${ssh_host_key}" sudo bash <<- EOL_SSH
	#!/usr/bin/env bash
	set -e -E -u -o pipefail

	trap 'echo "Error at script:\$LINENO. Command returned \$? : \$BASH_COMMAND" ; exit 255' ERR

	export BOX_HOSTS_ENTRIES=${BOX_HOSTS_ENTRIES@Q}
	export CONFIG_JMX_PORT=${CONFIG_JMX_PORT@Q}
	export CONFIG_LIFERAY_ARTIFACT_BASE=${CONFIG_LIFERAY_ARTIFACT_BASE@Q}
	export CONFIG_REPOS=${CONFIG_REPOS@Q}
	export CONFIG_THIS_ENV=${CONFIG_THIS_ENV@Q}
	export MAILHOG_BINARY_URL=${MAILHOG_BINARY_URL@Q}
	export ES_RPM_URL=${ES_RPM_URL@Q}

	export ASSETS_DIR='/vagrant/guest/assets'
	export CONFIG_MAP_FILE='config-${BOX_NAME}.map'


	# Stop Liferay, and disable starting with the server
	systemctl disable --now liferay

	bash /vagrant/guest/scripts/os-fixes.sh

	bash /vagrant/guest/scripts/configure-os.sh

	bash /vagrant/guest/scripts/mailhog.sh

	bash /vagrant/guest/scripts/elasticsearch.sh


	bash /vagrant/guest/scripts/liferay-fixes.sh

	bash /vagrant/guest/scripts/liferay-configs.sh


	bash /vagrant/guest/scripts/configure-to-oraclevm.sh \\
	  -f /apps/liferay/portal-ext.properties \\
	  --url '${ORACLE_CONNECTOR_JAR_URL}' \\
	  -h '${ORACLE_VM_HOST}' \\
	  -i '${ORACLE_VM_IP}' \\
	  -u '${CONFIG_ORACLE_SCHEMA_USER}' \\
	  -p '${CONFIG_ORACLE_SCHEMA_PASS}' \\
	  --hosts

	bash /vagrant/guest/scripts/configure-tomcat-jvm-memory.sh '${jvm_max_mem}'
EOL_SSH

# Reload the box so that startup Services will run
vagrant reload

vagrant snapshot save --force 'after-portal-provisioning'
#!/usr/bin/env bash

###################################################################
#
# mailhog.sh
#
#   Sets up MailHog (an email testing tool) as a service, and takes over Postfix.
#
#   Environment variables expected to be set:
#		MAILHOG_BINARY_URL		(URL to Mailhog binary)
#
###################################################################
# Exit immediately on non-zero status
set -e -E -u -o pipefail

# Uncomment to see commands that fail.
# trap 'echo "Error at ${BASH_SOURCE[0]}:$LINENO. Command returned $? : $BASH_COMMAND" ; exit 255' ERR


echo 'Installing MailHog'
cd /usr/local/bin
curl -sS --fail -o mailhog "$MAILHOG_BINARY_URL"
chmod 'u=rwx,go=r' mailhog

## Allow outside access to MailHog's Web UI
firewall-cmd --quiet --add-port=8025/tcp
firewall-cmd --quiet --add-port=8025/tcp --permanent

## Disable Postfix.
echo 'Disabling Postfix'
systemctl --now disable postfix

## Mailhog Service
tee /etc/systemd/system/mailhog.service <<-EOL_SERVICE > /dev/null
	[Unit]
	Description=MailHog, an SMTP intercept tool that displays received emails in a web UI and/or JSON API.
	Documentation=https://github.com/mailhog/MailHog/
	After=network.service
	After=vagrant.mount
	
	[Service]
	Type=simple
	ExecStart=/usr/local/bin/mailhog -smtp-bind-addr :25
	NonBlocking=true
	
	
	[Install]
	WantedBy=multi-user.target
EOL_SERVICE

systemctl daemon-reload
systemctl enable mailhog.service

exit 0
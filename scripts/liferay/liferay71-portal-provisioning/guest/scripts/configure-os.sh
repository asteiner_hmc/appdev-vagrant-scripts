#!/usr/bin/env bash

###################################################################
#
# configure-os.sh
#
#   Configures the OS to play nicer with Vagrant
#	This is meant to be run from the guest machine.
#
#   Environment variables expected to be set:
#		ASSETS_DIR
#		BOX_HOSTS_ENTRIES
#
###################################################################

# Exit immediately on non-zero status
set -e -E -u -o pipefail

# Uncomment to see commands that fail.
# trap 'echo "Error at ${BASH_SOURCE[0]}:$LINENO. Command returned $? : $BASH_COMMAND" ; exit 255' ERR


echo 'Configuring the "vagrant" user to have unlimited login sessions'
cp "${ASSETS_DIR}"/security/98-local-vagrant-security.conf /etc/security/limits.d/


eval "etc_hosts_entries=(${BOX_HOSTS_ENTRIES})"

if [ ${#etc_hosts_entries[@]} -ne 0 ]; then
	echo 'Configuring /etc/hosts'
	for entry in "${etc_hosts_entries[@]}"
	do
		echo "127.0.0.1   ${entry}" >> /etc/hosts
	done
fi

# Add the vagrant User to liferay-logs Group, so they can view logs without sudo
usermod --append --groups liferay-logs vagrant

exit 0

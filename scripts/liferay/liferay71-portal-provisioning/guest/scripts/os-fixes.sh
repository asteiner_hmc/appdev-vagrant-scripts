#!/usr/bin/env bash

set -e -E -u -o pipefail
###################################################################
#
# os-fixes.sh
#
#	Applys changes to the OS to fix behaviors caused by mis-configurations
#	of the base image.
# 
#	Ideally, we would find old behaviors with the box, fix them here, then
#	report them to DSA, who would fix them for the next image.  Then in the
#	next refresh run, we remove the fixes.
#
#	Environment variables expected to be set:
#		CONFIG_LIFERAY_ARTIFACT_BASE
#
#
###################################################################
# Fix: Ensure that the psh-liferay package was not installed from the pshsys-testing repo.
# Why: The psh-liferay package in the pshsys-testing repo (currently 2.0) does not mark psh-java8 as a dependency.
#      This results in psh-java8 being removed by 'yum autoremove', which is called by post-setup.sh. 

#if yum list installed psh-liferay | grep -q -P 'psh-liferay.*2\.0\.0-1.*@pshsys-testing'; then
#    # psh-liferay was in
#    echo 'psh-liferay v2.0 from the pshsys-testing repo is installed.  This version is known to cause issues due to an incorrect dependency configuration.'
#    echo ''
#    echo 'Removing this package, and installing the package from the pshsys repo...'
#    echo ''
#    yum remove --assumeyes psh-liferay psh-admin
#    yum install --assumeyes --disablerepo='*' --enablerepo=pshsys psh-liferay
#fi

# New Fix: Now psh-java8 get uninstalled regardless.  So reinstall these if pshsys-testing is detected.

#pkgs=('psh-liferay' 'psh-admin' 'psh-java8')
## Filter out packages not installed 
#for (( i=${#pkgs[@]}-1 ; i>=0 ; i-- )) ; do
#    if ! yum list installed "${pkgs[i]}"; then
#        unset 'pkgs[i]'
#    fi
#done
#
## If any of these packages were installed from the pshsys-testing repo
## Uninstall them and reinstall them from the pshsys repo
#if (( ${#pkgs[@]} > 0 )); then
#    if yum list installed "${pkgs[@]@Q}" | grep -q '@pshsys-testing'; then
#        yum remove --assumeyes "${pkgs[@]@Q}"
#        yum install --assumeyes --disablerepo='*' --enablerepo=pshsys "${pkgs[@]@Q}"
#    fi
#fi
# ATS 2022-11-09: The above code seems to cause more trouble than it's worth. also the psh-java8 package is not installed in 7.3 boxes.  So everythign seems to be ok for now. 

# Fix: Set LIFERAY_DEPLOY and LIFERAY_ARTIFACT_BASE in "/etc/psh/liferay/environment"
# Why: /apps/liferay-admin/sbin/deploy-plugins-lr7.sh was updated to read from these variables.
#      Upper environment servers were updated with these env vars after the current liferay71 box (at this moment 2020.02-1) was
#      published.
# The use of a single-quote here is intentional.
# shellcheck disable=SC2016
sed -i -r --follow-symlinks 's|(LIFERAY_DEPLOY=.*)|LIFERAY_DEPLOY="${LIFERAY_HOME}/deploy" #\1|g' /etc/psh/liferay/environment
sed -i -r --follow-symlinks "s|(LIFERAY_ARTIFACT_BASE=.*)|LIFERAY_ARTIFACT_BASE=${CONFIG_LIFERAY_ARTIFACT_BASE} #\1|g" /etc/psh/liferay/environment

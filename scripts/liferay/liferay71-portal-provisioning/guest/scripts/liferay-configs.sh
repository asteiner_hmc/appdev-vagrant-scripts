#!/usr/bin/env bash

###################################################################
#
#    Downloads and sets up configuration files for the Liferay 7.1 instance.
#
#    Environment variables expected to be set:
#        ASSETS_DIR              Path to assets folder provided by provisioning script.
#        CONFIG_MAP_FILE
#        CONFIG_REPOS            (String form of Associative array
#                                 Key:   Config Repo Name (in GitLab) ex: liferay-tomcat-intranet-v4
#                                 Value: Path to Repo on EIMSCM Host (ex: pshsys-cfg-local/liferay-tomcat-intranet-v4.git)
#        CONFIG_THIS_ENV         (ex: intranet or internet)
#        CONFIG_JMX_PORT         (ex: 50909)
#
#
# CONFIG_REPOS Example:
# "['liferay-tomcat-portal']='pshsys-cfg-local/liferay-tomcat-portal.git' ['liferay-portal']='pshcfg-local/liferay-portal.git'"
#
###################################################################

# Exit immediately on non-zero status
set -e -E -u -o pipefail

# Uncomment to see commands that fail.
# trap 'echo "Error at ${BASH_SOURCE[0]}:$LINENO. Command returned $? : $BASH_COMMAND" ; exit 255' ERR

source /packer-scripts/functions.sh

set +u
. /etc/profile
source /apps/liferay-admin/etc/functions.sh
set -u


echo 'Installing Liferay Configurations'
install -o root -g root -m 644 "${ASSETS_DIR}/configs/${CONFIG_MAP_FILE}"  /etc/psh/liferay/config.map


if [[ -n "${CONFIG_REPOS:-}" ]]; then
	git_host='eimscm.pennstatehealth.net'
	sudo -u pshcfg bash -c "ssh-keyscan ${git_host} >> /home/pshcfg/.ssh/known_hosts"
	eval "declare -A config_repos_arr=(${CONFIG_REPOS})"
	for repo_name in "${!config_repos_arr[@]}"; do
		repo_git_url="${config_repos_arr[$repo_name]}"
		# NOTE: If this command fails with an error like this:
		#   GitLab: The project you were looking for could not be found. fatal: Could not read from remote repository. Please make sure you have the correct access rights and the repository exists.
		# It's because the repo needs the "pshcfg" / Vagrant Config User's access Key enabled for that repo.
		# According to Alan (DSA), as of 2023-01-19, AppDev Maintainers of the repo can make this change.
		sudo /opt/psh/sbin/clone-configs.sh -g liferay "git@${git_host}:${repo_git_url}"
		sudo -u pshcfg /opt/psh/sbin/pull-configs.sh "${repo_name}"
	done

	sudo -u liferay install -d "$LIFERAY_HOME"/tomcat/lib/META-INF --mode=0755
	sudo -u liferay rm -f "$LIFERAY_HOME"/tomcat/conf/logging.properties
	sudo -u liferay rm -f "$LIFERAY_HOME"/portal-ext.properties

	/opt/psh/sbin/map-configs.sh

fi


echo 'Configuring Catalina'
install -o liferay -g liferay "${ASSETS_DIR}/tomcat/setenv.sh" "$LIFERAY_HOME"/tomcat/bin
sed -i --follow-symlinks "s/<PSH_PORTAL_KEY>/$CONFIG_THIS_ENV/g" "$LIFERAY_HOME"/tomcat/bin/setenv.sh
sed -i --follow-symlinks "s/<JMX_PORT>/$CONFIG_JMX_PORT/g" "$LIFERAY_HOME"/tomcat/bin/setenv.sh
firewall-cmd --quiet --add-port="$CONFIG_JMX_PORT"/tcp --permanent

firewall-cmd --quiet --add-port=7007/tcp --permanent

exit 0

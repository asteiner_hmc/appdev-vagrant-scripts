JAVA_HOME='/usr/java/jdk1.8.0'

# PSH Portal Key / Environment
JAVA_OPTS="$JAVA_OPTS -DpshPortalKey=<PSH_PORTAL_KEY>  -DpshEnvironment=local"

# 64-bit JVM
JAVA_OPTS="$JAVA_OPTS -server -d64 -Dorg.xml.sax.driver=com.sun.org.apache.xerces.internal.parsers.SAXParser"

# JVM Settings
MIN_MEM='1g'
MAX_MEM='2g'
MAX_METASPACE='512m'
#JVM_OPTS="-XX:NewSize=2048m -XX:MaxNewSize=2048m -Xms16g -Xmx16g -XX:MaxMetaspaceSize=1024m -XX:MetaspaceSize=1024m -XX:SurvivorRatio=20 -XX:TargetSurvivorRatio=90 -XX:MaxTenuringThreshold=15 -XX:+UseParNewGC -XX:ParallelGCThreads=8 -XX:ReservedCodeCacheSize=512m -XX:+UseConcMarkSweepGC -XX:+CMSParallelRemarkEnabled -XX:+CMSCompactWhenClearAllSoftRefs -XX:CMSInitiatingOccupancyFraction=85 -XX:+CMSScavengeBeforeRemark -XX:+CMSConcurrentMTEnabled -XX:ParallelCMSThreads=2 -XX:+UseCompressedOops -XX:+DisableExplicitGC -XX:-UseBiasedLocking -XX:+BindGCTaskThreadsToCPUs -XX:+UseFastAccessorMethods -javaagent:/apps/liferay/patching-tool/lib/patching-tool-agent.jar"
JVM_OPTS="-Xms${MIN_MEM} -Xmx${MAX_MEM} -XX:MaxMetaspaceSize=${MAX_METASPACE} -XX:SurvivorRatio=20 -XX:TargetSurvivorRatio=90 -XX:MaxTenuringThreshold=15 -XX:+UseParNewGC -XX:ParallelGCThreads=8                  -XX:ReservedCodeCacheSize=512m -XX:+UseConcMarkSweepGC -XX:+CMSParallelRemarkEnabled -XX:+CMSCompactWhenClearAllSoftRefs -XX:CMSInitiatingOccupancyFraction=85 -XX:+CMSScavengeBeforeRemark -XX:+CMSConcurrentMTEnabled -XX:ParallelCMSThreads=2 -XX:+UseCompressedOops -XX:+DisableExplicitGC -XX:-UseBiasedLocking -XX:+BindGCTaskThreadsToCPUs -XX:+UseFastAccessorMethods -javaagent:/apps/liferay/patching-tool/lib/patching-tool-agent.jar"
CATALINA_OPTS="$CATALINA_OPTS $JVM_OPTS"

# JMX Monitoring (uncomment to enable)
# JMX ports are set to the same port number used for VirtualBox's forwarding. JMX involves connecting a first time, then being told by the server to connect again on a given port using a given protocol.
# The server needs to tell the client the port number that VB is forwarding, not the actual port it's listening to. Unfortuately, the JVM's JMX implementation does not support this behavior.
JMX_PORT='<JMX_PORT>'
JMX_OPTS="-Dcom.sun.management.jmxremote -Dcom.sun.management.jmxremote.port=${JMX_PORT} -Dcom.sun.management.jmxremote.rmi.port=${JMX_PORT} -Dcom.sun.management.jmxremote.ssl=false -Dcom.sun.management.jmxremote.authenticate=false -Djava.rmi.server.hostname=localhost"
CATALINA_OPTS="$CATALINA_OPTS $JMX_OPTS"

# Remote Debugging (uncomment to enable)
JPDA_ADDRESS=7007
JPDA_TRANSPORT=dt_socket

# JGroups Settings (uncomment and update to enable)
#JGROUPS_OPTS="-Djgroups.bind_addr=<ipaddr> -Djgroups.tcpping.initial_hosts=<host1>[7800],<host2>[7800]"
#CATALINA_OPTS="$CATALINA_OPTS $JGROUPS_OPTS"

CATALINA_OPTS="$CATALINA_OPTS -Dfile.encoding=UTF8 -Djava.net.preferIPv4Stack=true -Dorg.apache.catalina.loader.WebappClassLoader.ENABLE_CLEAR_REFERENCES=false -Duser.timezone=GMT"

LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/lib64
export LD_LIBRARY_PATH
#!/usr/bin/env bash

###################################################################
#
#    Script to prepare a Liferay Vagrant VM to be packaged up.
#    This is meant to be run from the guest machine.
#
###################################################################

set -e -E -u -o pipefail

if [[ "$(whoami)" != 'root' ]]; then
	echo 'User must be root'
	exit 1
fi


# Clean up
echo 'Stopping Services'
# Not all these services may be on the instance we are compressing.
services=(
    'liferay'
    'mysqld'
    'openoffice'
    'httpd'
    'mailhog'
)

for service in "${services[@]}"; do
    echo "  $service > $(systemctl is-active "$service")"
    if systemctl -q is-active "$service"; then
        set +e
        echo "    Stopping $service"
        systemctl stop "${service}"
        set -e
    fi
done


## Application/Service -specific clean up
### ElasticSearch
if yum -q -C list installed elasticsearch > /dev/null; then
    echo 'ElasticSearch detected.  Clearing caches and temp files'
    systemctl start elasticsearch
    curl --silent -X POST 'localhost:9200/_cache/clear'
    systemctl stop elasticsearch
    es_home='/usr/share/elasticsearch'
    if [[ -d "${es_home}/tmp" ]]; then
        temp_dir_fd_count="$(find "${es_home}/tmp" -mindepth 1 -maxdepth 1 | wc -l)"
        echo "FYI: ${es_home}/tmp contains ${temp_dir_fd_count} files and directories."
        rm -rf "${es_home}"/tmp/*
    else
        echo "${es_home}/tmp directory does not exist. ElasticSearch temp file might not have been cleaned up"
    fi
fi

### Liferay
echo 'Cleaning out Liferay logs and work directories'
rm -rf /apps/liferay/logs/*
rm -rf /apps/liferay/tomcat/logs/*
rm -rf /apps/liferay/tomcat/work/*
rm -rf /apps/liferay/tomcat/temp/*
rm -rf /apps/liferay/osgi/state/*


echo 'Cleaning out Caches'
yum autoremove --assumeyes
yum clean all --assumeyes
rm -rf /var/cache/yum
rm -rf /tmp/* /var/tmp/*


# Remove Virtual Box Guest Additions ISO
rm -f /root/VBoxGuestAdditions.iso
rm -f /home/vagrant/VBoxGuestAdditions.iso


# Zero out empty space (Makes the packaged VM smaller)
echo 'Optimizing empty sectors for VM export'
while read -r mount_point ; do
    echo "Optimizing $mount_point"
    file="$(mktemp --tmpdir="$mount_point" EMPTY_XXX)"
    echo "Filling ${file}. Free Space available: $(df --si --output=avail "$mount_point" | sed 1d)"
    set +e
    dd if=/dev/zero of="${file}" bs=1M status=progress
    set -e
    rm -f "${file}"
    echo "Free space of $mount_point optimized."
done < <( lsblk  --list --noheadings --output mountpoint | grep -o '/.*' )


# Reset Vagrant SSH to unsecured public key (Will be replaced with secured, custom, private key)
echo 'Resetting "vagrant" SSH key'
mkdir -p /home/vagrant/.ssh
chmod 0700 /home/vagrant/.ssh
curl --fail -o /home/vagrant/.ssh/authorized_keys https://raw.githubusercontent.com/hashicorp/vagrant/main/keys/vagrant.pub
chmod 0600 /home/vagrant/.ssh/authorized_keys
chown -R vagrant /home/vagrant/.ssh

echo "WARNING: The Vagrant SSH Key has been replaced.  Further SSH Connections using the current key will fail." 

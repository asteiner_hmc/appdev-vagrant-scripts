#!/usr/bin/env bash

###################################################################
#
#    Changes the Max memory setting of Tomcat in a VM.  This script
#    assumes that there is a /apps/liferay/tomcat/bin/setenv.sh with
#    a MAX_MEM variable.
#
#    This is meant to be run from the guest machine.
#
#    Parameters
#        1    jvm_max_mem
#
###################################################################

set -e -E -u -o pipefail

jvm_max_mem="$1"

sudo -u liferay sed --regexp-extended --in-place --follow-symlinks "s/MAX_MEM='.*'/MAX_MEM='${jvm_max_mem}'/" /apps/liferay/tomcat/bin/setenv.sh
sudo -u liferay grep -E "MAX_MEM='.*'" /apps/liferay/tomcat/bin/setenv.sh


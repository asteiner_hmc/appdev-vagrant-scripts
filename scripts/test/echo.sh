#!/usr/bin/env bash

echo "This script is running as ${BASH_SOURCE[0]} on $(hostname)."

declare -p USER

for i in "$@"; do
	echo "arg: $i"
done

echo 'test_file.txt (Remote)'
cat /tmp/test_file.txt

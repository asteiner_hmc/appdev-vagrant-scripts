#!/usr/bin/env bash

set -e -u -o pipefail

# Uncomment to see commands that fail.
trap 'echo "Error at ${BASH_SOURCE[0]}:$LINENO. Command returned $? : $BASH_COMMAND" ; exit 255' ERR

# Get Directory that the script lives in
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

cd "${SCRIPT_DIR}"

props_array=(
	'array_prop_1'           'value1'
	'array_prop_2'           'value"2'
)

IFS='' read -r -d '' multilineVar2 <<EOL || true
hello
there
world
EOL

../publish-to-artifactory.sh  testbox123 \
  test_file.txt \
  2024.03-1 \
  'https://artifact.test.invalid/artifactory' \
  vagrant-pshdev-local \
  'prop1'        '2.4.1' \
  'prop2'        '7.0.14' \
  'multilineProp1'   $'hello\nworld' \
  'multilineProp2'   "${multilineVar2}" \
  'quotedProp1'      "'value'" \
  'quotedProp2'      '"value"' \
  "${props_array[@]}"

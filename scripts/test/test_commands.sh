#!/usr/bin/env bash
set -e -E -u -o pipefail

# Uncomment to see commands that fail.
trap 'echo "Error at ${BASH_SOURCE[0]}:$LINENO. Command returned $? : $BASH_COMMAND" ; exit 255' ERR

# Get Directory that the script lives in
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

cd "${SCRIPT_DIR}"

ORACLE_VM_VAGRANT_BOX_NAME="EIM_AppDev_Orcl_Web_Prod_v20240105_All_Schemas"
#vboxmanage showvminfo "${ORACLE_VM_VAGRANT_BOX_NAME}" --machinereadable | perl -ne '/^SharedFolderName.*?="(\w+)"/ && print "$1\0";'
#echo

mapfile -t -d $'\0' folders  < <(vboxmanage showvminfo "${ORACLE_VM_VAGRANT_BOX_NAME}" --machinereadable | perl -ne '/^SharedFolderName.*?="(\w+)"/ && print "$1\0";')
#declare -p folders
for sf_name in "${folders[@]}"; do
	echo "${sf_name}"
done

#while IFS= read -r -d '' sf_name ; do
#    echo "${sf_name}"
#done < <(vboxmanage showvminfo "${ORACLE_VM_VAGRANT_BOX_NAME}" --machinereadable | perl -ne '/^SharedFolderName.*?="(\w+)"/ && print "$1";')    

#!/usr/bin/env bash

case "$(uname -sr)" in
    Darwin*)
        # Mac OS X
        #For OSX, use coreutils if installed
        PATH="/usr/local/opt/coreutils/libexec/gnubin:$PATH"
        ;;

    CYGWIN*|MINGW*|MINGW32*|MSYS*)
        # CYGWIN on Windows
        ;;

    # Future Use
    Linux*Microsoft*)
        ;&
    Linux*)
        ;&
    *)
        # Other
        echo "OS $(uname -sr) is not supported by this script"
        exit 255
        ;;
esac


SCRIPT_DIR="$(cd "$(dirname "$(readlink -e "${BASH_SOURCE[0]}")")" && pwd)"

bash "${SCRIPT_DIR}"/../run-remote-script.sh \
 --vagrant-dir "${SCRIPT_DIR}"/../../states/combined_20230206T155626Z/vagrant/oracle19c-20230119 \
 --run-as oracle \
 --run-as-with-login-shell \
 --verbose \
 -- \
 "${SCRIPT_DIR}"/two-param-script.sh \
 --param-1 '' \
 --param-2 'oracle world'

#  Expect two-param-script.sh to run successfully on the oracle vm, and both parameters are passed in.
#
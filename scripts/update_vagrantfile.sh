#!/usr/bin/env bash
###################################################################
#
#    Creates a copy of the given Vagrant Box file, while replacing the Box's embedded Vagrantfile
#
#    Parameters
#        1    boxfile_path
#        2    new_vagrantfile_path
#
###################################################################

set -e -E -u -o pipefail

#For OSX, use coreutils if installed
PATH="/usr/local/opt/coreutils/libexec/gnubin:$PATH"

boxfile_full_path=$(grealpath "$1")
new_vagrantfile_file=$(grealpath "$2")

boxfile_dir=$(gdirname "${boxfile_full_path}")
boxfile_name=$(gbasename "${boxfile_full_path}")

ext="${boxfile_name##*.}"
name="${boxfile_name%.*}"

new_boxfile_full_path="${boxfile_dir}/${name}-updated.${ext}"

declare -p boxfile_full_path new_boxfile_full_path
tmp_dir=$(gmktemp --directory)
printf '%s\n' "Temp Working Directory: ${tmp_dir}"

cd "${tmp_dir}"
printf 'Opening Box\n'
gtar -xvzf "${boxfile_full_path}"
printf 'Replacing Vagrantfile\n'
gcp "${new_vagrantfile_file}" include/_Vagrantfile
printf 'Closing Box\n'
gtar -cvzf "${new_boxfile_full_path}" ./*

grm -rf --preserve-root=all "${tmp_dir}"
printf '%s\n' "New box file created at:  ${new_boxfile_full_path}"
#!/usr/bin/env bash

###################################################################
#
#    Packaging the box
#
#    Parameters
#        1    vagrant_dir
#        2    box_file       Convention for Artifactory is ${BOX_NAME}.x86_64_virtualbox$(date +%s).box
#        3    box_vagrant_file    (ex: /Users/asteiner/Dev/data/Ec4.8-g2.4/local-env-update-scripts/package-vagrants/infonet62_Vagrantfile )
###################################################################

set -e -E -u -o pipefail

vagrant_dir="$1"
box_file="$( realpath "$2" )"
box_vagrant_file="$( realpath "$3" )"

if [[ ! -e "${box_vagrant_file}" ]]; then
    echo "Vagrantfile ${box_vagrant_file} not found."
    exit 1
fi

if [ -f "${box_file}" ]; then
	echo "${box_file} already exists.  Deleting.  Sorry not sorry."
	rm -f "${box_file}"
fi


cd "${vagrant_dir}"

# Pass relative paths into 'vagrant' to cheaply get around os agnostic path translation
rel_box_file="$(realpath --relative-to=. "${box_file}" )"
rel_vagrant_file="$(realpath --relative-to=. "${box_vagrant_file}" )"
vbox_vm_name="$(cat .vagrant/machines/*/virtualbox/id)"


echo "Packaging up ${vagrant_dir} vagrant to ${box_file}"
# Note: It is important that --base is used here, despite the fact that we're packaging up a Vagrant box and technically --base is not needed.
# The flag stops Vagrant from packaging up the SSH keys currently in use for the box, which at this point are wrong since we delete them and insert the unsecured public key.
vagrant package --output  "${rel_box_file}" --vagrantfile "${rel_vagrant_file}" --base "${vbox_vm_name}"
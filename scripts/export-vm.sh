#!/usr/bin/env bash
###################################################################
#
#    Exports the given VM to an OVA or OVF (depemding on the file name given).
#
#    Parameters
#        1    CURRENT_VM_NAME
#        2    NEW_VM_NAME  Convention for Artifactory is ${BOX_NAME}.x86_64_virtualbox$(date +%s).box
#        3    RESULT_OVA
#
###################################################################

set -e -E -u -o pipefail

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

CURRENT_VM_NAME="$1"
NEW_VM_NAME="$2"
RESULT_OVA="$3"

bash "$SCRIPT_DIR"/shutdown-vm.sh "$CURRENT_VM_NAME"

sleep 2

function change_back() {
    # Change Name of Box back
    echo "Renaming VM back to '$CURRENT_VM_NAME'"
    VBoxManage modifyvm "$NEW_VM_NAME" --name "$CURRENT_VM_NAME"
}

if [[ "$CURRENT_VM_NAME" != "$NEW_VM_NAME" ]]; then
	# Change Name of Box
    echo "Renaming VM to '$NEW_VM_NAME'"
    VBoxManage modifyvm "$CURRENT_VM_NAME" --name "$NEW_VM_NAME"
    trap change_back EXIT
fi

if [[ -f "$RESULT_OVA" ]]; then
    echo "$RESULT_OVA already exists.  Deleting.  Sorry not sorry."
    rm -f "$RESULT_OVA"
fi

# Change to directory of OVA, and export it using the filename to use OS Agnostic Paths.
cd "$( dirname "${RESULT_OVA}" )"
echo "Exporting '$NEW_VM_NAME' to '$RESULT_OVA'"
vboxmanage export "$NEW_VM_NAME" --output "$( basename "${RESULT_OVA}" )"

#!/usr/bin/env bash
###################################################################
#
#    Script that uses Saxon to perform XSLT Transformations.
#    XSLT 3.0 / XPath 3.0 are supported
#
#    Parameters
#        1    XML File
#        2    XSL File
#        3    Output File
#
###################################################################

set -e -E -u -o pipefail

saxon_version='12.4'

# Bash Version check
#if [[ ${BASH_VERSINFO[0]} -lt 4 ]]; then
#	echo "This script requires Bash version >= 4"
#	exit 1
#fi

# See commands that fail.
trap 'echo "Error at line $LINENO. Command returned $? : $BASH_COMMAND" ; exit 255' ERR

# shellcheck disable=SC2059
case "$(uname -sr)" in
    Darwin*)
        # Mac OS X
        #For OSX, use coreutils if installed
        PATH="/usr/local/opt/coreutils/libexec/gnubin:$PATH"
        ;;

    CYGWIN*|MINGW*|MINGW32*|MSYS*)
        # CYGWIN on Windows
        ;;

    # Future Use
    Linux*Microsoft*)
        ;&
    Linux*)
        ;&
    *)
        # Other
        echo "OS $(uname -sr) is not supported by this script"
        exit 255
        ;;
esac

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# Look for directory in the Script's folder matching the Version
# This should contain Saxon's Jars
if [[ ! -d "${SCRIPT_DIR}/${saxon_version}" ]]; then
	# Download Saxon
	rm -f "${SCRIPT_DIR}"/saxon-he.zip
	# The Versions in Saxon's files look like "12-3"
	file_ver="${saxon_version//./-}"
	curl -q -L -o "${SCRIPT_DIR}"/saxon-he.zip https://github.com/Saxonica/Saxon-HE/releases/download/SaxonHE${file_ver}/SaxonHE${file_ver}J.zip
	unzip -q "${SCRIPT_DIR}"/saxon-he.zip '*.jar' -d "${SCRIPT_DIR}/${saxon_version}"	
fi

# Passing a relative path to "java" instead of an absolute path to be OS-agnostic
saxon_jar_path="$(realpath --relative-to=. "${SCRIPT_DIR}/${saxon_version}/saxon-he-${saxon_version}.jar")"

xml_path="$(realpath --relative-to=. "${1}")"
xsl_path="$(realpath --relative-to=. "${2}")"
output_path="$(realpath --relative-to=. "${3}")"

java -jar "${saxon_jar_path}" -s:"${xml_path}" -xsl:"${xsl_path}" -o:"${output_path}"

#!/usr/bin/env bash

###################################################################
#
#    Script that sets up the ORCL Vagrant with our desired configurations.
#
#    Parameters
#        1    vagrant_dir           Path to Oracle 19 Vagrant Directory
#        2    -r | --redo-psh-mods  If set, then the script rolls back the VM to the 'before-psh-modifications' snapshot, and performs all custom PSH-authored steps.
#
###################################################################

set -e -E -u -o pipefail

case "$(uname -sr)" in
	Darwin*)
		# Mac OS X
		#For OSX, use coreutils if installed
		PATH="/usr/local/opt/coreutils/libexec/gnubin:$PATH"
		;;

	CYGWIN*|MINGW*|MINGW32*|MSYS*)
		# CYGWIN on Windows
		;;

	# Future Use
	Linux*Microsoft*)
		;&
	Linux*)
		;&
	*)
		# Other
		echo "OS $(uname -sr) is not supported by this script"
		exit 255
		;;
esac

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

vagrant_dir="${1:-}"
# This must be filled in before running, but must always be empty when committing changes to Git

if [[ -z "${vagrant_dir}" ]]; then
	echo 'Please pass in the vagrant folder for oracle-vagrant-boxes/OracleDatabase/19.3.0'
	exit 1
fi

# Change vagrant_dir to the full path so that the "finally" function can clean up even if we change to a different directory.
vagrant_dir="$(realpath "${vagrant_dir}")"

cd "${vagrant_dir}"

install_oracle=1
if [[ "${2:-}" == '--redo-psh-mods' || "${2:-}" == '-r' ]]; then
	install_oracle=0
fi

if (( install_oracle == 1 )); then
	read -p 'Enter a password to use for the Database Administration Accounts (SYS, SYSTEM, etc): ' -s oracle_pwd
	printf '\n'

	read -p 'Reenter the password: ' -s oracle_pwd_2
	printf '\n'

	if [[ "${oracle_pwd}" != "${oracle_pwd_2}" ]]; then
		echo 'Passwords did not match.  Re-run to try again.'
		exit 2
	fi

	rm -rf ./userscripts/*.sql
	rm -rf ./userscripts/*.sh

	oracle_pdb='ORCL'

	# Add Script to Remove Password Expiration
	cat <<- EOL > ./userscripts/no_password_expire.sql
		-- Container: CDB\$ROOT
		ALTER PROFILE "DEFAULT" LIMIT PASSWORD_LIFE_TIME UNLIMITED;

		-- Change Passwords for Users, which resets the Password Expiration flags
		ALTER USER SYS IDENTIFIED BY "${oracle_pwd}" ACCOUNT UNLOCK;
		ALTER USER SYSTEM IDENTIFIED BY "${oracle_pwd}" ACCOUNT UNLOCK;

		-- Container: ${oracle_pdb}
		ALTER SESSION SET CONTAINER = ${oracle_pdb};
		ALTER PROFILE "DEFAULT" LIMIT PASSWORD_LIFE_TIME UNLIMITED;

		ALTER USER PDBADMIN IDENTIFIED BY "${oracle_pwd}" ACCOUNT UNLOCK;
	EOL


	echo 'Starting Oracle Vagrant.  Installing Oracle.  This will take a while.  Go get some coffee.'
	env VM_SYSTEM_TIMEZONE='UTC' VM_ORACLE_PDB="${oracle_pdb}" VM_ORACLE_PWD="${oracle_pwd}" VM_ORACLE_CHARACTERSET='AL32UTF8' vagrant up

	echo 'Stopping Box'
	vagrant halt

	echo 'Creating snapshot before proceeding'
	vagrant snapshot save --force 'before-psh-modifications'

else
	echo "Rolling back to 'before-psh-modifications'"
	vagrant snapshot restore --no-start 'before-psh-modifications'
fi

vm_name="$( cat .vagrant/machines/*/virtualbox/id )"

# Add a big drive to hold bigger schemas
bash "${SCRIPT_DIR}"/host/add-big-tablespace-disk.sh "${vm_name}"

# Enable USB 3 support
vboxmanage modifyvm "${vm_name}" --usbxhci on

vagrant up

cp -r "${SCRIPT_DIR}"/guest .
cp -r "${SCRIPT_DIR}"/../../vbga.sh ./guest

# Save vagrant ssh config info for easier scripting
echo "Getting SSH Config for Vagrant"
vagrant ssh-config > vagrant-ssh-config
function finish {
  rm -rf "${vagrant_dir}"/userscripts/*.sql
  rm -rf "${vagrant_dir}"/userscripts/*.sh
  rm -rf "${vagrant_dir}"/vagrant-ssh-config
  rm -rf "${vagrant_dir}"/guest
}
trap finish EXIT

# Extract the Host key Ex: Host: oracle-19c-vagrant
ssh_host_key="$(perl -ne 'exit if /^Host (\S*)\R?$/ && print "$1\n"' vagrant-ssh-config)"

echo 'Installing / Updating VirtualBox Guest Additions'

vbox_version="$(vboxmanage --version | perl -ne '/^(.*?)r/ && print "$1\n"')"
ssh -F vagrant-ssh-config "${ssh_host_key}" bash <<- EOL_SSH
	set -e  # If the following script fails, then return back to main script
	sudo bash /vagrant/guest/vbga.sh "${vbox_version}"
EOL_SSH

vagrant reload

echo 'Creating Big Tablespace Partition'
ssh -F vagrant-ssh-config "${ssh_host_key}" bash <<- EOL_SSH
	set -e  # If the following script fails, then return back to main script
	sudo bash /vagrant/guest/create-big-tablespace-partition.sh
EOL_SSH

#ssh_exit_code=$?
#set -e

#if (( $ssh_exit_code == 2 )); then
#	# create-big-tablespace-partition.sh was not successful
#	exit 1
#fi

echo 'Installing additional tools'
ssh -F vagrant-ssh-config "${ssh_host_key}" bash <<- EOL_SSH
	set -e  # If the following script fails, then return back to main script
	# Rsync needed for run-remote-script.sh, and also because it's rsync
	sudo yum install --assumeyes rsync
	# Samba needed for importing large Databases
	sudo yum install --assumeyes samba-client cifs-utils
EOL_SSH

finish

source "${SCRIPT_DIR}"/prep-and-package-oracle19c.sh .
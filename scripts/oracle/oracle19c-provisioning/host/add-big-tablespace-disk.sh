#!/usr/bin/env bash

###################################################################
#
#    Adds a new disk to the given VM, to act as the BigTablespace for ORCL.
#
#    Parameters
#        1    VM Name
#
###################################################################

set -e -E -u -o pipefail

is_cygwin=0
case "$(uname -sr)" in
    Darwin*)
        # Mac OS X
        #For OSX, use coreutils if installed
        PATH="/usr/local/opt/coreutils/libexec/gnubin:$PATH"
        ;;

    CYGWIN*|MINGW*|MINGW32*|MSYS*)
        # CYGWIN on Windows
        is_cygwin=1
        ;;

    # Future Use
    Linux*Microsoft*)
        ;&
    Linux*)
        ;&
    *)
        # Other
        echo "OS $(uname -sr) is not supported by this script"
        exit 255
        ;;
esac

vm_name="$1"

disk_size_in_GB=300

echo 'Looking for SATA controller on VM'
controller_index="$(vboxmanage showvminfo "${vm_name}" --machinereadable | perl -ne '/storagecontrollertype(\d+)="IntelAhci"/ && print "$1";' )"
controller_name=''
sata_port_count=0

if [[ -z "${controller_index}" ]]; then
    # No SATA Controller found.  Add one.
    controller_index=0
    vboxmanage storagectl "${vm_name}" --add sata --name "SATA Controller"
    controller_name='SATA Controller'
    sata_port_count=0
else
    # Get name of existing SATA Controller
    controller_name="$(vboxmanage showvminfo "${vm_name}" --machinereadable | perl -ne "/storagecontrollername${controller_index}=\"(.*)\"\R/ && print \"\$1\";" )"
    sata_port_count="$(vboxmanage showvminfo "${vm_name}" --machinereadable | perl -ne "/storagecontrollerportcount${controller_index}=\"(.*)\"\R/ && print \"\$1\";" )"
fi


vm_vbox_file="$(vboxmanage showvminfo "${vm_name}" | perl -ne '/Config file:\s*(.+)\R/  && print "$1";' )"
if (( $is_cygwin == 1)); then
    # If cygwin, Convert vm_vbox_file to Unix, drop the filename, then convert back to Windows
    vm_dir="$( cygpath -u "${vm_vbox_file}")"
    vm_dir="$( dirname "${vm_dir}")"
    os_vm_dir="$( cygpath -w "${vm_dir}")"
else
    vm_dir="$(dirname "${vm_vbox_file}")"
    os_vm_dir="${vm_dir}"
fi


# Unregister "${vm_folder}"/BigTablespace1.vmdk from VirtualBox if there was one already registered 
# Typically this would be from a previously botched run
set +e
echo "Cleaning up any leftover files.  You will probably see a VERR_FILE_NOT_FOUND error for \"${os_vm_dir}/BigTablespace1.vmdk\".  This is fine."
vboxmanage closemedium disk "${os_vm_dir}"/BigTablespace1.vmdk --delete
set -e


if [[ -e "${vm_dir}"/BigTablespace1.vmdk ]]; then
    echo 'deleting drive file manually'
    rm -f "${vm_dir}"/BigTablespace1.vmdk
fi


echo 'Creating BigTablespace1 Disk'
(( disk_size_in_MB = $disk_size_in_GB * 1024 ))
vboxmanage createmedium disk --size ${disk_size_in_MB}  --format VMDK --variant Standard --filename "${os_vm_dir}"/BigTablespace1.vmdk

echo 'Attaching BigTablespace1 Disk to VM'
vboxmanage storageattach "${vm_name}" --storagectl "${controller_name}" --type hdd --medium "${os_vm_dir}"/BigTablespace1.vmdk --port "${sata_port_count}"

echo "Attached BigTablespace1 Drive to ${controller_name}, Port ${sata_port_count}"
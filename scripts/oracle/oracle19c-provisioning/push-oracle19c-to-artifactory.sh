# !/usr/bin/env bash
###################################################################
#
#    Generates commands to publish the oracle19c box to Artifactory
#
#    Parameters
#        1    box_file  Path to packaged .box file
#
###################################################################

set -e -E -u -o pipefail

# Bash Version check
#if [[ ${BASH_VERSINFO[0]} -lt 4 ]]; then
#    echo "This script requires Bash version >= 4"
#    exit 1
#fi

# Uncomment to see commands that fail.
trap 'echo "Error at ${BASH_SOURCE[0]}:$LINENO. Command returned $? : $BASH_COMMAND" ; exit 255' ERR

# shellcheck disable=SC2221,SC2222
case "$(uname -sr)" in
    Darwin*)
        # Mac OS X
        #For OSX, use coreutils if installed
        PATH="/usr/local/opt/coreutils/libexec/gnubin:$PATH"
        ;;

    CYGWIN*|MINGW*|MINGW32*|MSYS*)
        # CYGWIN on Windows
        ;;

    # Future Use
    Linux*Microsoft*)
        ;&
    Linux*)
        ;&
    *)
        # Other
        echo "OS $(uname -sr) is not supported by this script"
        exit 255
        ;;
esac

# Get Directory that the script lives in
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# This info should be in the "boxinfo.txt" file in the directory where oracle19c.box was created.
box_os_kernel_version='5.4.17-2136.329.3.1.el7uek.x86_64' # uname -r
box_os_release_version='7.9' # cat /etc/os-release | grep -e '^VERSION='
box_os_name='Oracle Linux Server' # cat /etc/os-release | grep -e '^NAME='

oracle_version='19.3.0.0.0' # sql -S -L system  <<< 'SELECT VERSION_FULL FROM V$INSTANCE;'


"${SCRIPT_DIR}"/../../publish-to-artifactory.sh oracle19c \
  "$1" \
  2024.03-1 \
  'https://artifact.pennstatehealth.net/artifactory' \
  vagrant-pshdev-local \
  'vagrant.version'          '2.3.7' \
  'vagrant.provider_version' '7.0.14' \
  'os.name'                  "${box_os_name}" \
  'os.architecture'          'x86_64' \
  'os.release_full'          "${box_os_release_version}" \
  'kernel.name'              'Linux' \
  'kernel.release'           "${box_os_kernel_version}" \
  'oracle.version'           "${oracle_version}"
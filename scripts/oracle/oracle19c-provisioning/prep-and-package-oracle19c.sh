#!/usr/bin/env bash

###################################################################
#
#    Script that packages up an ORCL Vagrant, including running the common cleanup / vm compression scripts.
#
#    Parameters
#        1    vagrant_dir           Path to Oracle 19 Vagrant Directory
#        2    -r | --redo-psh-mods  If set, then the script rolls back the VM to the 'before-psh-modifications' snapshot, and performs all custom PSH-authored steps.
#
###################################################################

set -e -E -u -o pipefail

case "$(uname -sr)" in
    Darwin*)
        # Mac OS X
        #For OSX, use coreutils if installed
        PATH="/usr/local/opt/coreutils/libexec/gnubin:$PATH"
        ;;

    CYGWIN*|MINGW*|MINGW32*|MSYS*)
        # CYGWIN on Windows
        ;;

    # Future Use
    Linux*Microsoft*)
        ;&
    Linux*)
        ;&
    *)
        # Other
        echo "OS $(uname -sr) is not supported by this script"
        exit 255
        ;;
esac

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

vagrant_dir="${1:-}"

if [[ -z "${vagrant_dir}" ]]; then
    echo 'Please pass in the vagrant folder for oracle-vagrant-boxes/OracleDatabase/19.3.0'
    exit 1
fi

# Change vagrant_dir to the full path so that the "finally" function can clean up even if we change to a different directory.
vagrant_dir="$(realpath "${vagrant_dir}")"

cd "${vagrant_dir}"

# Save vagrant ssh config info for easier scripting
echo 'Getting SSH Config for Vagrant'
if [[ ! -e 'vagrant-ssh-config' ]]; then
    echo 'Getting SSH Config for Vagrant'
    vagrant ssh-config > vagrant-ssh-config
fi

ssh_host_key="$(perl -ne 'exit if /^Host (\S*)\R?$/ && print "$1\n"' vagrant-ssh-config)"

# Copy guest scripts into vagrant_dir so they can be executed.
cp -r "${SCRIPT_DIR}"/guest .

function finish {
  rm -rf "${vagrant_dir}"/vagrant-ssh-config
  rm -rf "${vagrant_dir}"/guest
}
trap finish EXIT


echo 'Box Information for Artifactory (also outputted to boxinfo.txt in the Vagrant Box directory)'
ssh -F vagrant-ssh-config "${ssh_host_key}" bash <<- "EOL_SSH" | tee boxinfo.txt
	echo "box_os_kernel_version='$(uname -r)'"
	echo "box_os_release_version='$(cat /etc/os-release | grep -e '^VERSION=')'"
	echo "box_os_name='$(cat /etc/os-release | grep -e '^NAME=')'"
EOL_SSH

echo 'Prepping Box for Packaging'
ssh -F vagrant-ssh-config "${ssh_host_key}" bash <<- EOL_SSH
	set -e  # If the following script fails, then return back to main script
	sudo bash /vagrant/guest/remove-old-uek-kernels.sh
	sudo bash /vagrant/guest/prep-oracle-vagrant-for-packaging.sh
EOL_SSH

bash "${SCRIPT_DIR}"/../../package-vagrant-box.sh . ./oracle19c.box "${SCRIPT_DIR}"/../../../package-vagrants/oracle19c_Vagrantfile

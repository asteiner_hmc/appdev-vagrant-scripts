# oracle-19c Vagrant

## Build New Box
*Run this when the Oracle 19c installation package has been updated (as of 2/6/2023, 19.3.0 is the latest), or if you need to change how ORCL is configured during installation.*

1. Clone the https://github.com/oracle/vagrant-projects repo.

1. Go to OracleDatabase/19.3.0, and follow the README to acquire the Oracle DB installation package for Linux.

1. Update `package-vagrants/oracle19c_Vagrantfile` with any changes needed.

1. Once the installation package is downloaded, run `build-oracle19c.sh`, passing in the path to *19.3.0*.

1. When finished, the following files will be created in the *19.3.0* directory:
	* oracle19c.box
	* boxinfo.txt

1. Open `push-oracle19c-to-artifactory.sh`, and change the following values:

    * box_version
    * box_virtualbox_version
    * box_vagrant_version
    * box_os_kernel_version (found in boxinfo.txt)
    * box_os_release_version (found in boxinfo.txt)

1. Run the modified `push-oracle19c-to-artifactory.sh`, passing in the path to `oracle19c.box`.  This will produce commands that publishes the box and sets properties.  Execute those as a User with publishing/updating permissions in Artifactory.


## Update Existing Box

1. Run `vagrant up` inside a directory for an `oracle19c` Vagrant box.

1. Perform whatever update actions needed for the box.

1. Run `prep-and-package-oracle19c.sh` on the host machine, passing in the path to the Vagrant box's directory.

1. When finished, the following files will be created in the directory:
	* oracle19c.box
	* boxinfo.txt

1. Open `push-oracle19c-to-artifactory.sh`, and change the following values:

    * box_version
    * box_virtualbox_version
    * box_vagrant_version
    * box_os_kernel_version (found in boxinfo.txt)
    * box_os_release_version (found in boxinfo.txt)

1. Run the modified `push-oracle19c-to-artifactory.sh`, passing in the path to `oracle19c.box`.  This will produce commands that publishes the box and sets properties.  Execute those as a User with publishing/updating permissions in Artifactory.

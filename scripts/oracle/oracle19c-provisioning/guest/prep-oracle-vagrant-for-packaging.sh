#!/usr/bin/env bash

###################################################################
#
#    Script to prepare an Oracle VM to be exported.
#    This is meant to be run from the guest machine.
#
###################################################################

set -e -E -u -o pipefail

if [[ "$(whoami)" != 'root' ]]; then
	echo 'User must be root'
	exit 1
fi


# Clean up
echo 'Stopping Services'
# Not all these services may be on the instance we are compressing.
services=(
    'oracle-rdbms'
)

for service in "${services[@]}"; do
    echo "  $service > $(systemctl is-active "$service")"
    if systemctl -q is-active "$service"; then
        set +e
        echo "    Stopping $service"
        systemctl stop "${service}"
        set -e
    fi
done

echo 'Cleaning out Caches'
yum autoremove --assumeyes
yum clean all --assumeyes
rm -rf /var/cache/yum
rm -rf /tmp/* /var/tmp/*


# Remove Virtual Box Guest Additions ISO
rm -f /root/VBoxGuestAdditions.iso
rm -f /home/vagrant/VBoxGuestAdditions.iso


# Zero out empty space (Makes the packaged VM smaller)
echo 'Optimizing empty sectors for VM export'
while read -r mount_point ; do
    echo "Optimizing $mount_point"
    file="$(mktemp --tmpdir="$mount_point" EMPTY_XXX)"
    echo "Filling ${file}. Free Space available: $(df --si --output=avail "$mount_point" | sed 1d)"
    set +e
    dd if=/dev/zero of="${file}" bs=1M status=progress
    set -e
    rm -f "${file}"
    echo "Free space of $mount_point optimized."
done < <( lsblk  --list --noheadings --output mountpoint | grep -o '/.*' )


# Reset Vagrant SSH to unsecured public key (Will be replaced with secured, custom, private key)
echo 'Resetting "vagrant" SSH key'
mkdir -p /home/vagrant/.ssh
chmod 0700 /home/vagrant/.ssh
curl --fail -o /home/vagrant/.ssh/authorized_keys https://raw.githubusercontent.com/hashicorp/vagrant/main/keys/vagrant.pub
chmod 0600 /home/vagrant/.ssh/authorized_keys
chown -R vagrant /home/vagrant/.ssh

echo "WARNING: The Vagrant SSH Key has been replaced.  Further SSH Connections using the current key will fail." 

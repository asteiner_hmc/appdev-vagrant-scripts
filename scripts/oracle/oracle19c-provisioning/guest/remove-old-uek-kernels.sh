#!/usr/bin/env bash

###################################################################
#
#    Script to remove old UEK Kernels from the VM.
#    These are not removed .
#
###################################################################

set -e -E -u -o pipefail

if [[ "$(whoami)" != 'root' ]]; then
	echo 'User must be root'
	exit 1
fi

# Remove all kernel-uek packages (including devel) that are not currently in use. 
rpm -qa | grep kernel-uek | grep -v "$(uname -r)" | xargs yum remove -y

package-cleanup --oldkernels --count=1
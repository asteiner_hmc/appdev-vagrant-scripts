#!/usr/bin/env bash

###################################################################
#
#    Script to mount a large partition in the VM to be used for our Oracle tablespaces
#
#    Parameters
#        1    Device to format and mount (Ex: sdc). Optional.  If not given, then the script will pick
#             a disk that is not mounted, and has roughly 300GB of space on it. 
#
###################################################################

set -e -E -u -o pipefail

base_drive="${1:-}" # Ex: sdc

disk_size_in_GB=300

if [[ -z "${base_drive}" ]]; then
    disk_size_lower_bound=$(( $disk_size_in_GB * 1000000000 )) 
    disk_size_upper_bound=$(( $disk_size_lower_bound + (50 * 1000000000) )) # lower bound + 50GB
    # Device path not specified.  Try to determine it ourselves
    # Read the list of disks on the system.  THe disk we want will
    # not be mounted (MOUNTPOINT=""), and 300GB
    declare -a possible_disks
    while read -r; do
        regexp='NAME="([^"]+)".*SIZE="([0-9]+)".*MOUNTPOINT=""'
        if [[ "$REPLY" =~ $regexp ]]; then
            size="${BASH_REMATCH[2]}"
            if (( $disk_size_lower_bound <= $size && $size <= $disk_size_upper_bound )); then
                 possible_disks+=("${BASH_REMATCH[1]}")
            fi
        fi
    done < <( lsblk --pairs --output NAME,SIZE,MOUNTPOINT --bytes )

    # if we found less than or more than 1 disk, then ask for user intervention
    if (( ${#possible_disks[@]} != 1 )); then #if possible_disks.length != 1
        printf '\n%s\n' "More than one disk was found to potentially be BigTablespace1."
        lsblk
        printf '%s\n\n' "Look over the following output, pick the correct disk, then run (on the Oracle VM):"
        printf '%s\n\n' "  bash $(readlink -e "${BASH_SOURCE[0]}") <NAME>"
        exit 2
    fi
    base_drive="${possible_disks[0]}"
fi

echo "Creating Partition for BigTablespace1 on /dev/${base_drive}"

parted "/dev/${base_drive}" mklabel gpt
parted -a opt "/dev/${base_drive}" mkpart primary ext4 0% 100%
mkfs.ext4 -L BigTablespace1 "/dev/${base_drive}1"

echo 'Mounting Partition at /BigTablespace1'
mkdir -p /BigTablespace1
mount -t ext4 -o defaults /dev/disk/by-label/BigTablespace1 /BigTablespace1
chown oracle:dba /BigTablespace1

# Clear out any previous mapping of /BigTablespace1
sed -i 's/^\/dev\/.* \/BigTablespace1 .*//g' /etc/fstab  # Overwrites original
echo '/dev/disk/by-label/BigTablespace1 /BigTablespace1 ext4 defaults 0 2' >> /etc/fstab

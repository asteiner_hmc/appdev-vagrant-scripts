#!/usr/bin/env bash
###################################################################
#
#    Enables the Vagrant User to be used for SSH Logins, and sets their password.
#    This is meant to be run from the guest machine.
#
#    Parameters
#        1    Vagrant User Password
#
###################################################################

set -e -E -u -o pipefail

# See commands that fail.
trap 'echo "Error at line $LINENO. Command returned $? : $BASH_COMMAND" ; exit 255' ERR

vagrant_user_password="$1"

# Enable SSH Password Authentication
sudo sed --in-place=bak -r --follow-symlinks 's/^\s*PasswordAuthentication no$/#\0/g' /etc/ssh/sshd_config
echo -e 'PasswordAuthentication yes' | sudo tee -a /etc/ssh/sshd_config > /dev/null

sudo systemctl restart sshd

# Set Vagrant User's password so that you can log in.
echo "${vagrant_user_password}" | sudo passwd --stdin vagrant

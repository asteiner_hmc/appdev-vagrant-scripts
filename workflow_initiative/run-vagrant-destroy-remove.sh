#!/usr/bin/env bash

###################################################################
#
#    Destroys Vagrant boxes in $STATE_FOLDER/vagrant and removes them from Vagrant's registry.
#
#    Parameters
#        1..N    vagrant_box_name (ewp62, ewp62-base, etc)
#
#    IMPORTANT:
#        This script is not meant to be run directly, but as an argument passed
#        in via global_run_with_workflow_vars.sh
###################################################################

set -e -E -u -o pipefail

if [ "$#" -lt 1 ]; then
	echo  "Please call with at least one argument"
	exit
else
	echo -e "\c"
fi


for vagrant_box_name in "$@"
do
	set +e
	echo "Destroying Vagrant at ${STATE_FOLDER}/vagrant/${vagrant_box_name}"
	cd "$STATE_FOLDER/vagrant/${vagrant_box_name}"
	vagrant destroy --force

	cd ..
	echo "Removing ${STATE_FOLDER}/vagrant/${vagrant_box_name}"
	rm -rf "${STATE_FOLDER}/vagrant/${vagrant_box_name}"

	echo "Removing Box ${vagrant_box_name}"
	vagrant box remove "${vagrant_box_name}" --force

	set -e
done

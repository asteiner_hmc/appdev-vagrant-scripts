#!/usr/bin/env bash

###################################################################
#
#
#    IMPORTANT:
#        This script is not meant to be run directly, but as an argument passed
#        in via global_run_with_workflow_vars.sh
###################################################################

set -e -E -u -o pipefail

#For OSX, use coreutils if installed
#PATH="/usr/local/opt/coreutils/libexec/gnubin:$PATH"


source "${STATE_FOLDER}"/vars-global.sh


new_vagrant_dir="${STATE_FOLDER}"/vagrant/"${ORACLE_VM_VAGRANT_BOX_NAME}"

echo "Provisioning new Oracle Vagrant (${ORACLE_VM_VAGRANT_BOX_NAME}) based on box ${ORACLE_VM_VAGRANT_BASE_BOX_NAME}"
bash "${SCRIPTS_DIR}"/vagrantup-box.sh --no-vagrant-up "${new_vagrant_dir}" "${ORACLE_VM_VAGRANT_BASE_BOX_NAME}"

cd "${new_vagrant_dir}"

# Use custom Vagrantfile
echo 'Writing custom Vagrantfile'
tee Vagrantfile <<- EOL_VAGRANTFILE > /dev/null
	
	Vagrant.configure("2") do |config|
	
	  config.vm.box = "${ORACLE_VM_VAGRANT_BASE_BOX_NAME}"
	
	  # Provider-specific configuration
	  config.vm.provider "virtualbox" do |vb|
	    vb.name = "${ORACLE_VM_VAGRANT_BOX_NAME}"
	    #vb.memory = "3072"
	    #vb.cpus = 2

	  end
	
	  config.vm.network "private_network", adapter: 2, ip: "${ORACLE_VM_IP}", netmask:"255.255.255.0"
	
	end
EOL_VAGRANTFILE

vagrant up

echo 'Saving SSH info to file'
vagrant ssh-config > vagrant-ssh-config

# Extract the Host key Ex: Host: oracle-19c-vagrant and save to file (Future utilization)
ssh_host_key="$(perl -ne 'exit if /^Host (\S*)\R?$/ && print "$1\n"' ./vagrant-ssh-config)"
echo "${ssh_host_key}" > vagrant-ssh-host

# Legacy location
cp vagrant-ssh-config "${STATE_FOLDER}"/vagrant-ssh-"${ORACLE_VM_VAGRANT_BOX_NAME}"

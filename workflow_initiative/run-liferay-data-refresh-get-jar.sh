#!/usr/bin/env bash

###################################################################
#
#    Downloads the liferay-data-refresh jar and puts it to <STATE_FOLDER>/ldr/liferay-data-refresh-*.jar
#
#    IMPORTANT:
#        This script is not meant to be run directly, but as an argument passed
#        in via global_run_with_workflow_vars.sh
###################################################################

set -e -E -u -o pipefail

# See commands that fail.
trap 'echo "Error at line $LINENO. Command returned $? : $BASH_COMMAND" ; exit 255' ERR


source "${STATE_FOLDER}"/vars-global.sh ''

output_dir="${STATE_FOLDER}"/ldr

mkdir -p "${output_dir}"

echo 'Downloading liferay-data-refresh jar'
curl  -L --output-dir "${output_dir}" --remote-name "${LDR_URL}" || { echo 'liferay-data-refresh jar did not download correctly.'; exit 1; }

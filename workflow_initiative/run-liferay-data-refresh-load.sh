#!/usr/bin/env bash

###################################################################
#
#    Runs liferay-data-refresh loadAll and allPostMigrateTasks against the database of the given portal.
#    Script assumes the LDR jar is at <STATE_FOLDER>/ldr/liferay-data-refresh-*.jar (put there by run-liferay-data-refresh-get-jar.sh)
#
#    Parameters
#        1    Portal Name (infonet71, infonet73, etc)
#
#    IMPORTANT:
#        This script is not meant to be run directly, but as an argument passed
#        in via global_run_with_workflow_vars.sh
###################################################################

set -e -E -u -o pipefail

case "$(uname -sr)" in
    Darwin*)
        # Mac OS X
        #For OSX, use coreutils if installed
        PATH="/usr/local/opt/coreutils/libexec/gnubin:$PATH"
        ;;

    CYGWIN*|MINGW*|MINGW32*|MSYS*)
        # CYGWIN on Windows
        ;;

    # Future Use
    Linux*Microsoft*)
        ;&
    Linux*)
        ;&
    *)
        # Other
        echo "OS $(uname -sr) is not supported by this script"
        exit 255
        ;;
esac

portal_name="$1"

source "${STATE_FOLDER}"/vars-global.sh "${portal_name}"

# Build directories for Portal-specific run of LDR
ldr_portal_work_dir="${STATE_FOLDER}/ldr/${portal_name}local"

run_dir="${ldr_portal_work_dir}"/load-run
mkdir -p "${run_dir}"
rm -rf --preserve-root "${run_dir:?}"/*

config_file_path="${run_dir}/config.groovy"
echo "Building config.groovy at ${config_file_path}"


tee "${config_file_path}" <<-EOL > /dev/null
	application = 'liferay-data-refresh'

	liferayDataRefresh {
	    environment = 'LOCAL'
	    appHomeDir = '.'
	    appHomeArchiveDir = 'archive'
	    whitelistEmailAddressEpassIds = ['this_user_does_not_exist']
	    ${portal_name}local {
	        driverClass = 'oracle.jdbc.OracleDriver'
	        jdbcUrl = 'jdbc:oracle:thin:@localhost:1521/orcl'
	        username = '${CONFIG_ORACLE_SCHEMA_USER}'
	        password = '${CONFIG_ORACLE_SCHEMA_PASS}'
	        hosts = [
	            ${LDR_HOSTS_BLOCK}
	        ]
	    }
	}
EOL

## LDR moves the extracted configs to an archive folder when finished.
## So we make a copy before running so that subsequent runs of the task can be performed.
extracted_dir="${run_dir}"/extracted
cp -r -f "${ldr_portal_work_dir}"/extracted "${extracted_dir}"

## Run LDR from the <portal-name>local/run directory, which now contains a config.groovy that psh-commons-config in LDR will use.
cd "${run_dir}"

# Passing a relative path to "java" instead of an absolute path to be OS-agnostic
ldr_jar_path="$(realpath --relative-to=. "${STATE_FOLDER}"/ldr/liferay-data-refresh-*.jar)"
extract_key="$(realpath --relative-to=. "${extracted_dir}" )"

"${LDR_JAVA_HOME}"/bin/java -jar "${ldr_jar_path}" --loadAll "${portal_name}local" "${extract_key}"
"${LDR_JAVA_HOME}"/bin/java -jar "${ldr_jar_path}" --allPostMigrateTasks "${portal_name}local"

#!/usr/bin/env bash

###################################################################
#
#    Restarts the Liferay instance on the given Vagrant box.
#
#    Parameters
#        1    VAGRANT_BOX_NAME (ewp62, ewp62-base, infonet71, etc)
#
#    Outputs
#        states/.../vagrant/liferay62-portal-$BOX_NAME.box
#
#    IMPORTANT:
#        This script is not meant to be run directly, but as an argument passed
#        in via global_run_with_workflow_vars.sh
###################################################################

set -e -E -u -o pipefail

VAGRANT_BOX_NAME="$1"

# Spin up
echo 'Restarting Liferay Service'
ssh -F "$STATE_FOLDER/vagrant-ssh-$VAGRANT_BOX_NAME" default <<- "EOL"
	sudo systemctl restart liferay
EOL

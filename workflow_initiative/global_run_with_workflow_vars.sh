#!/usr/bin/env bash

###################################################################
#
#    Reads state variables from given STATE_ID into the shell, and executes the given workflow script.
#    Loads global functions as well.
#
#    Makes STATE_FOLDER and SCRIPTS_DIR available
#
#    Parameters
#        1    STATE_ID
#        2    RUN_SCRIPT
#
###################################################################

set -e -E -u -o pipefail

# shellcheck disable=SC2221,SC2222
case "$(uname -sr)" in
    Darwin*)
        # Mac OS X
        #For OSX, use coreutils if installed
        PATH="/usr/local/opt/coreutils/libexec/gnubin:$PATH"
        ;;

    CYGWIN*|MINGW*|MINGW32*|MSYS*)
        # CYGWIN on Windows
        ;;

    # Future Use
    Linux*Microsoft*)
        ;&
    Linux*)
        ;&
    *)
        # Other
        echo "OS $(uname -sr) is not supported by this script"
        exit 255
        ;;
esac

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

export ENVUPDATE_PROJECT_DIR="$( cd "$SCRIPT_DIR"/.. && pwd )"

export SCRIPTS_DIR="$ENVUPDATE_PROJECT_DIR"/scripts
WORKFLOW_STEPS_DIR="$ENVUPDATE_PROJECT_DIR"/workflow_initiative


STATE_ID="$1"
RUN_SCRIPT="$2"

export STATE_FOLDER="${ENVUPDATE_PROJECT_DIR}/states/${STATE_ID}"
if [[ ! -d "${STATE_FOLDER}" ]]; then
    echo "Workflow State ${STATE_ID} does not seem to exist. Exiting."
    exit 2
fi

# loadStateVars
# Deprecated - I don't want to write and read variables during the process anymore.
# It makes is hard to pause and continue the process.
# However, I do not have an alternative.  There are times where I need to extract info in one step, and use it in another.
while IFS= read -r -d '' varfile ; do \
	varname="$(basename "$varfile" | perl -pe 's/VAR_([_A-Za-z0-9]*)(?:\.txt)?/\1/' )" ; \
	#echo "Loading $varname from $varfile" ; \
	declare -x "$varname"="$(cat "$varfile")" ; \
done < <(find "${STATE_FOLDER}" -maxdepth 1 -type f -name 'VAR_*' -print0)
# find's output is fed into a named pipe, which is read by while
# It is done this way instead of "find ... | while ..." , because the latter runs while in a subshell, which keeps the variable declarations.
# The former runs while in the current shell
# ::bangs head against keyboard::
#


source "$WORKFLOW_STEPS_DIR"/global_workflow_functions.sh

shift # Move parameters over, taking out STATE_ID
shift # Move parameters over, taking out RUN_SCRIPT

. "$RUN_SCRIPT"
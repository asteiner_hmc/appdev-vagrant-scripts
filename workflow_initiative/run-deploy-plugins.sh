#!/usr/bin/env bash

###################################################################
#
#    Deploys 7.1/7.3 plugins to the Vagrant box.
#
#    Parameters
#        1    portal_name (infonet71, infonet73, etc)
#        1    vagrant_box_name (infonet71-base, etc)
#
#    IMPORTANT:
#        This script is not meant to be run directly, but as an argument passed
#        in via global_run_with_workflow_vars.sh
###################################################################

set -e -E -u -o pipefail

# Uncomment to see commands that fail.
trap 'echo "Error at ${BASH_SOURCE[0]}:$LINENO. Command returned $? : $BASH_COMMAND" ; exit 255' ERR

portal_name="$1"
vagrant_box_name="$2"

# Bring in vars
source "$STATE_FOLDER"/vars-global.sh "${portal_name}"

## Code to download the deploy-plugins.ctl, using as little of extra repo files as possible
function getDeployPluginsControlFile() {

	repo_origin="$1"
	version="$2"
	copy_file_to="$3"

	# Use Sparse Checkout to keep the number of repo files created low, as we only need one file.
	# Perform the Sparce Checkout in a folder NOT under local-env-update-scripts, so the sparse-checkout does not
	# affect the current Git repo. 
	tmp_dir="$(mktemp --directory)"
	pushd "${tmp_dir}"
	git clone --no-checkout --depth 1 --branch v"${version}" "${repo_origin}" git-sparse
	cd git-sparse
	git sparse-checkout set psh-release/deploy-plugins.ctl
	git checkout
	cp psh-release/deploy-plugins.ctl "${copy_file_to}"
	popd
	rm -rf "${tmp_dir}"
}

echo "Downloading deploy-plugins.ctl for ${PORTAL_THIS_RELEASE_VERSION}"
deploy_plugins_ctl_file="${STATE_FOLDER}"/vagrant/"${vagrant_box_name}"/deploy-plugins.ctl
getDeployPluginsControlFile "${PORTAL_THIS_RELEASE_GIT_REPO}" "${PORTAL_THIS_RELEASE_VERSION}" "${deploy_plugins_ctl_file}"

ssh -F "$STATE_FOLDER/vagrant-ssh-${vagrant_box_name}" default <<- "EOL_SSH"
	sudo -u liferay /apps/liferay-admin/sbin/deploy-plugins-lr7.sh /vagrant/deploy-plugins.ctl
EOL_SSH

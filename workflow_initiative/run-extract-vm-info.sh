#!/usr/bin/env bash

###################################################################
#
#    Extracts information from the VM, including Kernel and Fixpack
#
#    Parameters
#        1    PORTAL_NAME (ewp62, infonet71, etc)
#        2    VAGRANT_BOX_NAME (ewp62, ewp62-base, infonet71, etc)
#
#    Outputs
#        Workflow Variable BOX_OS_KERNEL_VERSION_<PORTAL_NAME (uppercase)>        Kernel Version string from 'uname'
#        Workflow Variable BOX_OS_RELEASE_VERSION_<PORTAL_NAME (uppercase)>       Kernel Version string from 'uname'
#
#    IMPORTANT:
#        This script is not meant to be run directly, but as an argument passed
#        in via global_run_with_workflow_vars.sh
###################################################################

set -e -E -u -o pipefail

PORTAL_NAME="$1"
VAGRANT_BOX_NAME="$2"

declare "BOX_OS_KERNEL_VERSION_${PORTAL_NAME^^}"="$(ssh -F "${STATE_FOLDER}"/vagrant-ssh-"${VAGRANT_BOX_NAME}" default uname -r )"
saveStateVar "BOX_OS_KERNEL_VERSION_${PORTAL_NAME^^}"

declare "BOX_OS_RELEASE_VERSION_${PORTAL_NAME^^}"="$(ssh -F "${STATE_FOLDER}"/vagrant-ssh-"${VAGRANT_BOX_NAME}" default cat /etc/redhat-release | grep -o -P '\d+\.\d+' )" # Expecting redhat-release to return: "Red Hat Enterprise Linux Server release 7.7 (Maipo)"
saveStateVar "BOX_OS_RELEASE_VERSION_${PORTAL_NAME^^}"
#!/usr/bin/env bash

###################################################################
#
#    IMPORTANT:
#        This script is not meant to be run directly, but as an argument passed
#        in via global_run_with_workflow_vars.sh
###################################################################

set -e -E -u -o pipefail

VAGRANT_BOX_NAME="$1"

ssh -F "$STATE_FOLDER/vagrant-ssh-$VAGRANT_BOX_NAME" default <<- "EOL_SSH"
	sudo -u liferay tail -c +0 -f /apps/liferay/tomcat/logs/catalina.out
EOL_SSH
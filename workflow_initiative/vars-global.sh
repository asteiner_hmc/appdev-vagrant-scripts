#!/usr/bin/env bash
set -e -E -u -o pipefail

trap 'echo "Error at line $LINENO. Command returned $? : $BASH_COMMAND" ; exit 255' ERR

case "$(uname -sr)" in
    Darwin*)
        # Mac OS X
        #For OSX, use coreutils if installed
        PATH="/usr/local/opt/coreutils/libexec/gnubin:$PATH"
        ;;

    CYGWIN*|MINGW*|MINGW32*|MSYS*)
        # CYGWIN on Windows
        ;;

    # Future Use
    Linux*Microsoft*)
        ;&
    Linux*)
        ;&
    *)
        # Other
        echo "OS $(uname -sr) is not supported by this script"
        exit 255
        ;;
esac

# Optional
portal_name="${1:-}"


##################################################################################################
# Passwords
# This section should be commited to Git WITH EMPTY VALUES ALWAYS.
##################################################################################################
PASS__ORACLE_ORCL_SYSTEM=''
PASS__ORACLE_SCHEMA_LIFERAY=''
PASS__ORACLE_SCHEMA_MED=''

##################################################################################################
# Variables that change with every run.
##################################################################################################
export BOX_VERSION='2024.04.0'  #Box Version

PLUGINS_VERSION_INFONET71_PORTAL='3.11.0'  # infonet71 Intranet Plugins Version
PLUGINS_VERSION_INFONET73_PORTAL='4.11.0'  # infonet73 Intranet Plugins Version
PLUGINS_VERSION_EWP73_PORTAL='3.7.0'      # ewp73 Internet Plugins Version

export APPDEV_ORCL_WEB_PROD_DATE='20240411'  # Production Export Date String. Must be safe for filenames.

BOX_LIFERAY_FIXPACK_VERSION_71='28' # Liferay Fixpack Number
BOX_LIFERAY_HOTFIX_VERSION_71='liferay-hotfix-7060-7110--fp28-DupEmbPort-SamlUrlFrag'

BOX_LIFERAY_FIXPACK_VERSION_73='33'  # Liferay Update Number
BOX_LIFERAY_HOTFIX_VERSION_73=''

##################################################################################################
# Variables that periodically change.
##################################################################################################
# URL of Liferay Data Refresh binary to use.
LDR_VERSION='1.5.0-SNAPSHOT'
export LDR_URL="https://artifact.pennstatehealth.net/artifactory/app-snapshot/pshdev/liferay-data-refresh/${LDR_VERSION}/liferay-data-refresh-${LDR_VERSION}.jar"
export BOX_VIRTUALBOX_VERSION='7.0.14'
export BOX_VAGRANT_VERSION='2.4.1'
## Not actually used.  Will eventually use this in the future.
#export ARTIFACTORY_BASE_URL='https://artifact.pennstatehealth.net/artifactory'
#export ARTIFACTORY_BASE_URL='http://gateway:8080/downloads'

##################################################################################################
# Variables that change depending on the machine these scripts are running on.
##################################################################################################

# OSX: "$(/usr/libexec/java_home -v 11)"
# CYGWIN: "$(cygpath 'C:\\Program Files\\Java\jdk_11')"
export LDR_JAVA_HOME="$(cygpath 'C:\\jdk\\11')"
export ORACLE_DUMP_DIR_HOST_PATH='/cygdrive/s/DBDumps'  # Directory on Host Machine of DBDumps Shared Folder (Historically: /Volumes/Scratch_Space_500/DBDumps)
export DBDMP_ARCHIVES_PATH='/cygdrive/a/ArchivedDBDumps'  # Dump File Archive Directory (Historically: /Volumes/Scratch_Space)
export VBOX_EXPORT_PATH='/cygdrive/s'  # Where to place VM exports (Not packaged Vagrant boxes) (Historically: /Volumes/Scratch_Space_500)

##################################################################################################
# Variables that rarely change.
##################################################################################################
#export VBOX_HOST_IP='10.0.2.2'

export ORACLE_VM_HOST='oracle-vm-local'
export ORACLE_VM_IP='192.168.60.199'

export ORACLE_VM_VAGRANT_BASE_BOX_NAME='oracle19c' # Vagrant box used as base of Oracle VM
export ORACLE_VM_VAGRANT_BOX_NAME="${ORACLE_VM_VAGRANT_BASE_BOX_NAME}-${APPDEV_ORCL_WEB_PROD_DATE}"

export ORACLE_ORCL_USER_USER='oracle'
export ORACLE_ORCL_SID='ORCL'

export ORACLE_ORCL_SYSTEM_USER='system'
export ORACLE_ORCL_SYSTEM_PASS="${PASS__ORACLE_ORCL_SYSTEM}"
export ORACLE_BIG_TABLESPACE_PATH='/BigTablespace1'

export ORACLE_VM_VBOX_NAME="AppDev_Orcl_Web_Prod_v${APPDEV_ORCL_WEB_PROD_DATE}" # Base Name of Exported Oracle VM
export ORACLE_VM_EXPORTED_VBOX_NAME="${ORACLE_VM_VBOX_NAME}_Full"

export MAILHOG_BINARY_URL='https://artifact.pennstatehealth.net/artifactory/INSTALL/mailhog/Linux/x86_64/1.0.0/MailHog_linux_amd64'

# Future Change
# Find '/media/SS_500_USB'; Replace with ${ORACLE_VM_GUEST_SCRATCH_PATH}
export ORACLE_VM_GUEST_SCRATCH_PATH='/media/SS_500_USB'
# Find ORACLE_VM_DUMPS_FOLDER; Replace with ORACLE_VM_GUEST_DUMPS_PATH
export ORACLE_VM_GUEST_DUMPS_PATH='/media/SS_500_USB/DBDumps'
##################################################################################################
# Per-portal variables.
##################################################################################################

function vars_infonet71 () {
	export BOX_NAME='infonet71'
	export BOX_FRIENDLY_NAME='Infonet' # TODO Not Used. Remove on next run
	export BOX_VAGRANT_FILE="$ENVUPDATE_PROJECT_DIR"/package-vagrants/"$BOX_NAME"_Vagrantfile

	export CONFIG_THIS_ENV='intranet'
	export CONFIG_JMX_PORT='50304'
	export CONFIG_REPOS="['liferay-tomcat-7.1']='pshsys-cfg-local/liferay-tomcat-7.1.git' ['liferay-intranet-7.1']='pshcfg-local/liferay-intranet-7.1.git'"

	export BOX_HOSTS_ENTRIES="'infonet-local' 'psh-infonet-local'"

	export PORTAL_THIS_RELEASE_GIT_REPO='git@bitbucket.org:pshdev/portals-intranet_liferay-plugins-sdk-7.0.git'
	export PORTAL_THIS_RELEASE_VERSION="${PLUGINS_VERSION_INFONET71_PORTAL}"

	## Database
	export SRC_DMP_TABLESPACE_NAME='INFONET' # Name of Tablespace found in the Source DMP files
	export SRC_DMP_SCHEMA_USER='INFONET71' # Schema Name found in the Source DMP files
	export SRC_DMP_FILE_NAME='INTWEB_exp_infonet71.dmp' # File name of DMP file used during the import.

	export CONFIG_ORACLE_SCHEMA_USER='INFONET71'
	export CONFIG_ORACLE_SCHEMA_PASS="${PASS__ORACLE_SCHEMA_LIFERAY}"

	# Excludes Document Previews
	#QUERY=DLCONTENT:"WHERE PATH_ NOT LIKE 'document_preview/%' AND PATH_ NOT LIKE 'document_thumbnail/%'"

	# export IMPORT_PAR_EXCLUDE="$(cat <<- EOL
	# 	QUERY='SAMLIDPSPCONNECTION:"WHERE ROWNUM = 0"'
	# 	QUERY='SAMLIDPSPSESSION:"WHERE ROWNUM = 0"'
	# 	QUERY='SAMLIDPSSOSESSION:"WHERE ROWNUM = 0"'
	# 	QUERY='SAMLSPAUTHREQUEST:"WHERE ROWNUM = 0"'
	# 	QUERY='SAMLSPIDPCONNECTION:"WHERE ROWNUM = 0"'
	# 	QUERY='SAMLSPMESSAGE:"WHERE ROWNUM = 0"'
	# 	QUERY='SAMLSPSESSION:"WHERE ROWNUM = 0"'
	# EOL
	# )"
	export IMPORT_PAR_EXCLUDE=''

	export LDR_HOSTS_BLOCK="$(cat <<- EOL
		'infonet.pennstatehershey.net': 'infonet-local:50300',
		'infonet.pennstatehealth.net': 'psh-infonet-local:50300',
	EOL
	)"

	vars_71
}

function vars_71 () {
	export BOX_LIFERAY_PLATFORM_VERSION='7.1.10'
	export BOX_LIFERAY_FIXPACK_VERSION="$BOX_LIFERAY_FIXPACK_VERSION_71"
	export BOX_LIFERAY_HOTFIX_VERSION="$BOX_LIFERAY_HOTFIX_VERSION_71"
	## This variable is used to fix the liferay71 box's LIFERAY_ARTIFACT_BASE environment variable.
	## See liferay-base-packer-template/7.1/liferay71-portal/scripts/liferay-fixes.sh
	## This variable should not be changed to use a local web server, as this value is persisted with the box.
	export CONFIG_LIFERAY_ARTIFACT_BASE='https://artifact.pennstatehealth.net/artifactory/libs-release'

	## Database
	export ORACLE_CONNECTOR_JAR_URL='https://artifact.pennstatehealth.net/artifactory/ext-release-local/com/oracle/jdbc/ojdbc8/12.2.0.1/ojdbc8-12.2.0.1.jar'

	export ES_RPM_URL='https://artifact.pennstatehealth.net/artifactory/rpm-x86_64-ext-release-el7/elasticsearch-6.8.22.rpm'
}

function vars_infonet73 () {
	export BOX_NAME='infonet73'
	export BOX_FRIENDLY_NAME='Infonet' # TODO Not Used. Remove on next run
	export BOX_VAGRANT_FILE="${ENVUPDATE_PROJECT_DIR}"/package-vagrants/"${BOX_NAME}"_Vagrantfile

	export CONFIG_THIS_ENV='intranet'
	export CONFIG_JMX_PORT='50604'
	export CONFIG_REPOS="['liferay-tomcat-intranet-v4']='pshsys-cfg-local/liferay-tomcat-intranet-v4.git' ['liferay-intranet-v4']='pshcfg-local/liferay-intranet-v4.git'"

	export BOX_HOSTS_ENTRIES="'infonet3-local'"

	export PORTAL_THIS_RELEASE_GIT_REPO='git@bitbucket.org:pshdev/portals-liferay-intranet-v4.git'
	export PORTAL_THIS_RELEASE_VERSION="${PLUGINS_VERSION_INFONET73_PORTAL}"

	## Database
	export SRC_DMP_TABLESPACE_NAME='INFONET' # Name of Tablespace found in the Source DMP files
	export SRC_DMP_SCHEMA_USER='INFONET73' # Schema Name found in the Source DMP files
	export SRC_DMP_FILE_NAME='INTWEB_exp_infonet73.dmp' # File name of DMP file used during the import.

	export CONFIG_ORACLE_SCHEMA_USER='INFONET73'
	export CONFIG_ORACLE_SCHEMA_PASS="${PASS__ORACLE_SCHEMA_LIFERAY}"

	# Excludes Document Previews
	#QUERY=DLCONTENT:"WHERE PATH_ NOT LIKE 'document_preview/%' AND PATH_ NOT LIKE 'document_thumbnail/%'"

	export IMPORT_PAR_EXCLUDE=''

	export LDR_HOSTS_BLOCK="$(cat <<- EOL
		'infonet.pennstatehershey.net': 'infonet3-local:50600',
	EOL
	)"

	vars_73
}

function vars_ewp73 () {
	export BOX_NAME='ewp73'
	export BOX_FRIENDLY_NAME='Public Portal' # TODO Not Used. Remove on next run
	export BOX_VAGRANT_FILE="${ENVUPDATE_PROJECT_DIR}"/package-vagrants/"${BOX_NAME}"_Vagrantfile

	export CONFIG_THIS_ENV='internet'
	export CONFIG_JMX_PORT='50404'
	export CONFIG_REPOS="['liferay-tomcat-internet-v3']='pshsys-cfg-local/liferay-tomcat-internet-v3.git' ['liferay-internet-v3']='pshcfg-local/liferay-internet-v3.git'"

	export BOX_HOSTS_ENTRIES="'cancer-local' 'com-local'"

	export PORTAL_THIS_RELEASE_GIT_REPO='git@bitbucket.org:pshdev/portals-liferay-internet-v3.git'
	export PORTAL_THIS_RELEASE_VERSION="${PLUGINS_VERSION_EWP73_PORTAL}"

	## Database
	export SRC_DMP_TABLESPACE_NAME='EWP' # Name of Tablespace found in the Source DMP files
	export SRC_DMP_SCHEMA_USER='EWP73' # Schema Name found in the Source DMP files
	export SRC_DMP_FILE_NAME='EXTWEB_exp_ewp73.dmp' # File name of DMP file used during the import.

	export CONFIG_ORACLE_SCHEMA_USER='EWP73'
	export CONFIG_ORACLE_SCHEMA_PASS="${PASS__ORACLE_SCHEMA_LIFERAY}"

	# Excludes Document Previews
	#QUERY=DLCONTENT:"WHERE PATH_ NOT LIKE 'document_preview/%' AND PATH_ NOT LIKE 'document_thumbnail/%'"

	export IMPORT_PAR_EXCLUDE=''

	export LDR_HOSTS_BLOCK="$(cat <<- EOL
        'cancer.psu.edu': 'cancer-local:50400',
        'med.psu.edu': 'com-local:50400',
	EOL
	)"

	vars_73
}

function vars_73 () {
	export BOX_LIFERAY_PLATFORM_VERSION='7.3.10'
	export BOX_LIFERAY_FIXPACK_VERSION="${BOX_LIFERAY_FIXPACK_VERSION_73}"
	export BOX_LIFERAY_HOTFIX_VERSION="${BOX_LIFERAY_HOTFIX_VERSION_73}"
	## This variable is used to fix the liferay73 box's LIFERAY_ARTIFACT_BASE environment variable.
	## See liferay-base-packer-template/7.3/liferay73-portal/scripts/liferay-fixes.sh
	## This variable should not be changed to use a local web server, as this value is persisted with the box.
	export CONFIG_LIFERAY_ARTIFACT_BASE='https://artifact.pennstatehealth.net/artifactory/libs-release'

	## Database
	export ORACLE_CONNECTOR_JAR_URL='https://artifact.pennstatehealth.net/artifactory/ext-release-local/com/oracle/jdbc/ojdbc8/12.2.0.1/ojdbc8-12.2.0.1.jar'

	export ES_RPM_URL='https://artifact.pennstatehealth.net/artifactory/rpm-x86_64-ext-release-el7/elasticsearch-7.17.19-x86_64.rpm'
}

function vars_med () {
	## Database
	export SRC_DMP_TABLESPACE_NAME='MED' # Name of Tablespace found in the Source DMP files
	export SRC_DMP_SCHEMA_USER='MED' # Schema Name found in the Source DMP files
	export SRC_DMP_FILE_NAME='EXTWEB_exp_med.dmp' # File name of DMP file used during the import.  Deprecated.

	export CONFIG_ORACLE_SCHEMA_USER='MED'
	export CONFIG_ORACLE_SCHEMA_PASS="${PASS__ORACLE_SCHEMA_MED}"

	export IMPORT_PAR_EXCLUDE=''
}

case "$portal_name" in
	'')
		# No Portal. Likely just using the params for the Oracle VM
		;;
	infonet71)  vars_infonet71 ;;
	infonet73)  vars_infonet73 ;;
	ewp73)      vars_ewp73 ;;
	med)        vars_med ;;
	*)
		echo "Unknown portal \"${portal_name}\"" #> /dev/stderr
		exit 1
		;;
esac

##################################################################################################
# Variables determined from Portal-specific Variables
##################################################################################################
if [[ -n "${CONFIG_ORACLE_SCHEMA_USER:-}" ]]; then
	export CONFIG_ORACLE_SCHEMA_TABLESPACE="${CONFIG_ORACLE_SCHEMA_USER}"
fi


# Clear out our ERR trap so other scripts don't get it.
trap - ERR

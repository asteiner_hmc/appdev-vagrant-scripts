<?xml version="1.0"?>
<xsl:transform version="3.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	>
	<xsl:output
		method="html"
		encoding="UTF-8"
		indent="no"
		omit-xml-declaration="yes"
	/>

	<xsl:template match="/">
		<html>
			<head>
				<link rel="icon" href="data:," />
				<!-- https://cdnjs.com/libraries/jquery -->
				<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.1/jquery.min.js" integrity="sha512-v2CJ7UaYy4JwqLDIrZUI/4hqeoQieOmAZNXBeQyjo21dadnwR+8ZaIJVT8EE2iyI61OV8e6M8PP2/4hpQINQ/g==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
				<script src="https://cdnjs.cloudflare.com/ajax/libs/prism/1.29.0/prism.min.js" integrity="sha512-7Z9J3l1+EYfeaPKcGXu3MS/7T+w19WtKQY/n+xzmw4hZhJ9tyYmcUS+4QqAlzhicE5LAfMQSF3iFTK9bQdTxXg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
				<script src="https://cdnjs.cloudflare.com/ajax/libs/prism/1.29.0/components/prism-bash.min.js" integrity="sha512-whYhDwtTmlC/NpZlCr6PSsAaLOrfjVg/iXAnC4H/dtiHawpShhT2SlIMbpIhT/IL/NrpdMm+Hq2C13+VKpHTYw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
				<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/prism/1.29.0/themes/prism-tomorrow.min.css" integrity="sha512-vswe+cgvic/XBoF1OcM/TeJ2FW0OofqAVdCZiEYkd6dwGXthvkSFWOoGGJgS2CW70VK5dQM5Oh+7ne47s74VTg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
				<script src="https://cdnjs.cloudflare.com/ajax/libs/prism/1.29.0/plugins/autolinker/prism-autolinker.min.js" integrity="sha512-h92O152CCXt3xEUWDYTGLz58u+IOpZU8Z2MEkkBsXsRlAhckPFeEolarVn7tOhTVbjsJPpyknL0CFUrc2rlgPQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
				<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/prism/1.29.0/plugins/autolinker/prism-autolinker.min.css" integrity="sha512-4ZmAB2UXPu3Rgy5ZClpqnJ/zXXZBdulFXY1eWMLgIjp2HWgkHGIpr1b7kmCK+rdD5NYfivTp47UR+bQ4oTBllQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
				<style><![CDATA[
					body {
						font-family: Arial;
						font-size: 14px;
						background: black;
						color: silver;
						padding-left: 20px;
					}

					h1,h2,h3,h4,h5,h6 {
						color: orange;
					}

					ol {
						padding-left: 20px;
					}

					li::marker {
						font-size: 18px;
					}

					li:only-child {
						list-style-type: none;
					}

					pre, code {
						font-family: Consolas;
					}

					pre[class*="language-"] {
						border-radius: 10px;
						overflow: visible;
						margin: 0;
					}

					.action-container {
						display: flex;
						gap: 10px;
						flex-wrap: nowrap;
						margin-bottom: 1em;

						.checkbox-div {
							flex:none;
							margin-top: 1em;
						}

						.action-completed-checkbox {
							cursor: pointer;
							width:  32px;
							height: 32px;
							margin: 0 0 0 0;
							vertical-align: middle;
						}

						.copy-btn {
							flex: none;
							cursor: pointer;
							border-radius: 1px;
							width: 32px;
							height: 32px;
							margin: 1em 0 0 0;

							span {
								font-size: 20px;
							}
						}

						textarea {
							display: none;
						}

					}

					.action-completed {
						.copy-btn {
							background-color: #2eb82e;
							border-color: #2eb82e;
							border-radius: 1px;
						}
						pre {
							text-decoration: line-through;
							color: darkGrey;
						}
					}



				]]></style>
				<script><![CDATA[
					async function copyCode(text) {
						await navigator.clipboard.writeText(text);
					}

					function toggleActionCompleted(/* Element */ actionContainerEle, /* Boolean */ isCompleted, /* Boolean */ updateLocalStorage) {
						$(actionContainerEle).toggleClass('action-completed', isCompleted);

						if (updateLocalStorage) {
							window.localStorage.setItem('action' + actionContainerEle.id + '.isCompleted', isCompleted.toString());
						}
					}

					function clearAllCompletionStates() {
						if (window.confirm('Are you sure you want to reset all "Action Completed" states?')) {
							window.localStorage.clear();
							$('.action-container').each(function(){
								$('#' + this.id + ' .action-completed-checkbox').prop( "checked", false);
								toggleActionCompleted(this, false, false);
							});
						}
					}

					$(function() {
						$('.action-container').each(function(){
							var isCompleted = window.localStorage.getItem('action' + this.id + '.isCompleted') === 'true'
							$('#' + this.id + ' .action-completed-checkbox').prop( "checked", isCompleted);
							toggleActionCompleted(this, isCompleted, false);
						});

						// Make all LOCAL links open in new windows
						$('a[href*="-local"]').attr('target', '_blank');
					});
				]]></script>
			</head>
			<body>
				<div>Last built: <xsl:value-of  select="current-date()"/> - <xsl:value-of select="current-time()"/></div>
				<div>
					<button onclick="clearAllCompletionStates();">Reset Completion States of Actions</button>
				</div>
				<ol>
					<xsl:apply-templates />
				</ol>
			</body>
		</html>
	</xsl:template>

	<xsl:template match="step">
		<li>
			<xsl:if test="./label">
				<xsl:element name="h{count(ancestor::*) + 1}"><xsl:value-of select="./label[1]" /></xsl:element>
			</xsl:if>
			<ol>
				<xsl:apply-templates />
			</ol>
		</li>
	</xsl:template>

	<xsl:template match="step/label" />

	<xsl:template match="action">
		<!-- strip-margin="true" will remove all leading white space characters followed by a pipe character ('|') from every line.   -->
		<xsl:variable name="code_L1">
			<xsl:choose>
				<xsl:when test="@strip-margin='true'">
					<xsl:value-of select="replace(., '^[ \t]*\|', '', 'm')" />
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="." />
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<!-- By default, all leading and ending white space are removed from output.  Setting trim="false" disables this behavior. -->
		<xsl:variable name="code_L2">
			<xsl:choose>
				<xsl:when test="@trim='false'">
					<xsl:value-of select="$code_L1" />
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="replace($code_L1, '^\s*(.+?)\s*$', '$1', 's')" />
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="pos" select="generate-id(.)" />
		<li>
			<div class="action-container" id="{$pos}">
				<div class="checkbox-div">
					<input
						type="checkbox"
						title="Action Completed"
						class="action-completed-checkbox"
						onchange="toggleActionCompleted(document.getElementById('{$pos}'), this.checked, true);"
					/>
				</div>
				<xsl:choose>
					<xsl:when test="@type='manual'">
						<pre><code class="language-text"><xsl:value-of select="$code_L2" /></code></pre>
					</xsl:when>
					<xsl:otherwise>
						<button class="copy-btn" title="Copy" onclick="copyCode($(this).siblings('textarea').get(0).value)">
							<span>⧉</span>
						</button>
						<textarea><xsl:value-of select="$code_L2" /></textarea>
						<pre><code class="language-bash"><xsl:value-of select="$code_L2" /></code></pre>
					</xsl:otherwise>
				</xsl:choose>
			</div>
		</li>
	</xsl:template>
</xsl:transform>
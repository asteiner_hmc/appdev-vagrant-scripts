#!/usr/bin/env bash
###################################################################
#
#    Parameters
#        1    PORTAL_NAME (ewp62 OR infonet71)
#        2    SRC_BOX_FILE_NAME (ex: infonet71.box)
#        3    NEW_BOX_NAME (ex: infonet71-base, infonet71-base2, etc)
#
#    Outputs
#        states/.../vagrant/liferay62-portal-$BOX_NAME.box
#
#    IMPORTANT:
#        This script is not meant to be run directly, but as an argument passed
#        in via global_run_with_workflow_vars.sh
###################################################################

set -e -E -u -o pipefail

portal_name="$1"
new_box_name="$2"

source "${STATE_FOLDER}"/vars-global.sh "${portal_name}"

new_box_dir="${STATE_FOLDER}/vagrant/${new_box_name}"

bash "${SCRIPTS_DIR}"/vagrantup-box.sh \
  --vagrant-up \
  "${new_box_dir}" \
  "${new_box_name}"

cd "${new_box_dir}"

echo 'Saving SSH info to file'
vagrant ssh-config > vagrant-ssh-config

# Extract the Host key Ex: Host: oracle-19c-vagrant and save to file (Future utilization)
ssh_host_key="$(perl -ne 'exit if /^Host (\S*)\R?$/ && print "$1\n"' ./vagrant-ssh-config)"
echo "${ssh_host_key}" > vagrant-ssh-host

# Legacy location
cp vagrant-ssh-config "${STATE_FOLDER}"/vagrant-ssh-"${new_box_name}"

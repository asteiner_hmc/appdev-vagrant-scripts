#!/usr/bin/env bash
###################################################################
#
#    Parameters
#        1    PORTAL_NAME (ewp62 OR infonet71)
#        2    VAGRANT_BOX_NAME (ewp62, ewp62-base, infonet71, etc)
#
#    Outputs
#        states/.../vagrant/$VAGRANT_BOX_NAME.box
#
#    IMPORTANT:
#        This script is not meant to be run directly, but as an argument passed
#        in via global_run_with_workflow_vars.sh
###################################################################

set -e -E -u -o pipefail

PORTAL_NAME="$1"
VAGRANT_BOX_NAME="$2"

source "$STATE_FOLDER"/vars-global.sh "${PORTAL_NAME}"

bash "${SCRIPTS_DIR}"/package-vagrant-box.sh \
	"$STATE_FOLDER"/vagrant/"$VAGRANT_BOX_NAME" \
	"$STATE_FOLDER"/vagrant/"$BOX_NAME".box \
	"$BOX_VAGRANT_FILE"

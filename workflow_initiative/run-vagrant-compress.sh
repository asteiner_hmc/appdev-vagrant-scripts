#!/usr/bin/env bash

###################################################################
#
#    Prepares the Liferay Vagrant for packaging
#
#    IMPORTANT:
#        This script is not meant to be run directly, but as an argument passed
#        in via global_run_with_workflow_vars.sh
###################################################################

set -e -E -u -o pipefail

VAGRANT_BOX_NAME="$1"

( cd "${STATE_FOLDER}"/vagrant/"${VAGRANT_BOX_NAME}" ; vagrant snapshot save --force 'before-compress' )

bash "${SCRIPTS_DIR}"/run-remote-script.sh \
  --ssh-config-file "${STATE_FOLDER}/vagrant-ssh-${VAGRANT_BOX_NAME}" \
  --run-as root \
  -- \
  "${SCRIPTS_DIR}"/liferay/prep-liferay-vagrant-for-packaging.sh

vbox_vm_name="$(cat "${STATE_FOLDER}"/vagrant/"${VAGRANT_BOX_NAME}"/.vagrant/machines/*/virtualbox/id )"
bash "${SCRIPTS_DIR}"/shutdown-vm.sh "${vbox_vm_name}"

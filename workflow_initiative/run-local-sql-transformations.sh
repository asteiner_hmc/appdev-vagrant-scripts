#!/usr/bin/env bash

###################################################################
#
#    IMPORTANT:
#        This script is not meant to be run directly, but as an argument passed
#        in via global_run_with_workflow_vars.sh
###################################################################

set -e -E -u -o pipefail

portal_name="$1"

source "${STATE_FOLDER}"/vars-global.sh "${portal_name}"

echo 'run_sqls.sh -- LOCAL-Specific Transformations'
ssh -F "${STATE_FOLDER}"/vagrant-ssh-"${ORACLE_VM_VAGRANT_BOX_NAME}" default sudo -u "${ORACLE_ORCL_USER_USER}" -i sql -S -L "${CONFIG_ORACLE_SCHEMA_USER}/${CONFIG_ORACLE_SCHEMA_PASS}@${ORACLE_ORCL_SID}" < "${ENVUPDATE_PROJECT_DIR}"/convert-prod-dmp-to-local/local_transformations/01-Local-Specific.sql

echo 'run_sqls.sh -- "Wipe" Address Table'
ssh -F "${STATE_FOLDER}"/vagrant-ssh-"${ORACLE_VM_VAGRANT_BOX_NAME}" default sudo -u "${ORACLE_ORCL_USER_USER}" -i sql -S -L "${CONFIG_ORACLE_SCHEMA_USER}/${CONFIG_ORACLE_SCHEMA_PASS}@${ORACLE_ORCL_SID}" < "${ENVUPDATE_PROJECT_DIR}"/convert-prod-dmp-to-local/local_transformations/02-WipeAddressTable.sql

echo 'run_sqls.sh -- Transformations complete.  Cleaning up'
echo '--------------------------------------------------------------------'

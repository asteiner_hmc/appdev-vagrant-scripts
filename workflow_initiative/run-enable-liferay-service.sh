#!/usr/bin/env bash

###################################################################
#
#    Enables the Liferay Service on the Vagrant box.
#
#    Parameters
#        1    BOX_NAME (ewp62, ewp62-base, etc)
#
#    IMPORTANT:
#        This script is not meant to be run directly, but as an argument passed
#        in via global_run_with_workflow_vars.sh
###################################################################

set -e -E -u -o pipefail

BOX_NAME="$1"

echo 'Enabling Liferay Service'
ssh -F "$STATE_FOLDER/vagrant-ssh-$BOX_NAME" default <<- "EOL"
	sudo systemctl enable liferay
EOL
#!/usr/bin/env bash
###################################################################
#
#    Configures JVM Memory
#
#    Parameters
#        1    Box Name               (ewp62, infonet71, etc)
#        2    JVM Max Memory  Configures the "-Xmx" value used in setenv.sh.  Ex: 2g, 4g
#
#    IMPORTANT:
#        This script is not meant to be run directly, but as an argument passed
#        in via global_run_with_workflow_vars.sh
###################################################################

set -e -E -u -o pipefail

# Bash Version check
#if [[ ${BASH_VERSINFO[0]} -lt 4 ]]; then
#	echo "This script requires Bash version >= 4"
#	exit 1
#fi

# See commands that fail.
trap 'echo "Error at line $LINENO. Command returned $? : $BASH_COMMAND" ; exit 255' ERR

case "$(uname -sr)" in
    Darwin*)
        # Mac OS X
        #For OSX, use coreutils if installed
        PATH="/usr/local/opt/coreutils/libexec/gnubin:$PATH"
        ;;

    CYGWIN*|MINGW*|MINGW32*|MSYS*)
        # CYGWIN on Windows
        ;;

    # Future Use
    Linux*Microsoft*)
        ;&
    Linux*)
        ;&
    *)
        # Other
        echo "OS $(uname -sr) is not supported by this script"
        exit 255
        ;;
esac


# Uppercase variables imply they are set outside of this script
# Lowercase variable imply that are used only in this script
vagrant_box_name="$1"

jvm_max_mem="$2"

bash "${SCRIPTS_DIR}"/run-remote-script.sh \
  --ssh-config-file "${STATE_FOLDER}/vagrant-ssh-${vagrant_box_name}" \
  -- \
  "${SCRIPTS_DIR}"/liferay/configure-tomcat-jvm-memory.sh \
  "${jvm_max_mem}"



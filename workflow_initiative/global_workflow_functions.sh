#!/usr/bin/env bash
#function outputVars() {
#	OUTPUT_FILE_KEY="$1"
#	declare -a VARS_TO_OUTPUT=("${!2}")
#
#	OUTPUT_FILE="$STATE_FOLDER/vars-output-$OUTPUT_FILE_KEY.sh"
#
#	echo '#!/usr/bin/env bash' > $OUTPUT_FILE
#	for v in "${VARS_TO_OUTPUT[@]}" ; do
#		echo "export $(declare -p $V | sed -n 's/^declare -- //p')" > $OUTPUT_FILE
#	done
#}

function saveStateVar() {
	VAR_NAME="$1"
	echo -n "${!VAR_NAME}" > "$STATE_FOLDER"/VAR_"$VAR_NAME".txt
}

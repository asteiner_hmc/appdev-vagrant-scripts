#!/usr/bin/env bash

###################################################################
#
#    Compress DMP files
#
#    Parameters
#        1    portal_name
#
#    IMPORTANT:
#        This script is not meant to be run directly, but as an argument passed
#        in via global_run_with_workflow_vars.sh
###################################################################

set -e -E -u -o pipefail

# Bash Version check
#if [[ ${BASH_VERSINFO[0]} -lt 4 ]]; then
#	echo "This script requires Bash version >= 4"
#	exit 1
#fi

# See commands that fail.
trap 'echo "Error at line $LINENO. Command returned $? : $BASH_COMMAND" ; exit 255' ERR

case "$(uname -sr)" in
    Darwin*)
        # Mac OS X
        #For OSX, use coreutils if installed
        PATH="/usr/local/opt/coreutils/libexec/gnubin:$PATH"
        ;;

    CYGWIN*|MINGW*|MINGW32*|MSYS*)
        # CYGWIN on Windows
        ;;

    # Future Use
    Linux*Microsoft*)
        ;&
    Linux*)
        ;&
    *)
        # Other
        echo "OS $(uname -sr) is not supported by this script"
        exit 255
        ;;
esac


# Uppercase variables imply they are set outside of this script
# Lowercase variable imply that are used only in this script
portal_name="$1"

# Bring in vars for this portal
source "$STATE_FOLDER"/vars-global.sh "${portal_name}"
# Or if you are not specific to any portal
#source "$STATE_FOLDER"/vars-global.sh '' 

# Grab a dmp file in ORACLE_DUMP_DIR_HOST_PATH that starts with 'exp_<CONFIG_ORACLE_SCHEMA_USER>_' and 
src_file="$(ls "$ORACLE_DUMP_DIR_HOST_PATH/${SRC_DMP_FILE_NAME}")"
# Drop the 'exp_' prefix
compressed_file="$(basename "$src_file" | sed -E 's:^exp_::' )"
compressed_file="$DBDMP_ARCHIVES_PATH/$compressed_file.gz"

echo "Compressing ${src_file} to ${compressed_file}"
pv -bepr "${src_file}" | gzip > "${compressed_file}"
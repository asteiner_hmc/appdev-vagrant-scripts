#!/usr/bin/env bash

###################################################################
#
#    Starts a new workflow to provision updated Liferay Vagrant Boxes and related Oracle DB VM.
#
#    Parameters
#        1    STATE_ID   Optional.  Sets the STATE_ID if passed in, otherwise generated via timestamp.  Mainly used for testing workflow changes.
#
###################################################################

set -e -E -u -o pipefail

# shellcheck disable=SC2221,SC2222
case "$(uname -sr)" in
    Darwin*)
        # Mac OS X
        #For OSX, use coreutils if installed
        PATH="/usr/local/opt/coreutils/libexec/gnubin:${PATH}"
        ;;

    CYGWIN*|MINGW*|MINGW32*|MSYS*)
        # CYGWIN on Windows
        ;;

    # Future Use
    Linux*Microsoft*)
        ;&
    Linux*)
        ;&
    *)
        # Other
        echo "OS $(uname -sr) is not supported by this script"
        exit 255
        ;;
esac

function toOSPath() {
	if [[ "$(uname -sr)" =~ CYGWIN.*|MINGW.*|MINGW32.*|MSYS.* ]]; then
		cygpath -m "$1" ;
	else
		printf '%s' "$1" ;
	fi
}

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

export ENVUPDATE_PROJECT_DIR="$( cd "${SCRIPT_DIR}/.." && pwd )"

export SCRIPTS_DIR="${ENVUPDATE_PROJECT_DIR}"/scripts
export WORKFLOW_STEPS_DIR="${ENVUPDATE_PROJECT_DIR}"/workflow_initiative

if [[ -n "${1:-}" ]]; then
	export STATE_ID="$1"
else
	export STATE_ID="combined_$(date +%Y_%m_%d__%H_%M_%SZ)"
fi

export STATE_FOLDER="${ENVUPDATE_PROJECT_DIR}/states/${STATE_ID}"
if [[ -n "${1:-}" && -d "${STATE_FOLDER}" ]]; then
	# Custom STATE_ID was passed in, and the directory exists.
	# Assume we are debugging, so remove the existing directory
	echo "Deleting existing state folder \"${STATE_FOLDER}\""
	rm -r -f "${STATE_FOLDER}"
fi

mkdir -p "${STATE_FOLDER}"


source "${WORKFLOW_STEPS_DIR}"/vars-global.sh ''

function getPortalVar() {
	source "${WORKFLOW_STEPS_DIR}"/vars-global.sh "$1"
	printf '%s\n' "${!2}"
}

# Copy vars-global
cp "${WORKFLOW_STEPS_DIR}"/vars-global.sh "${STATE_FOLDER}"


echo 'Generating Workflow Commands'

# Future, Maybe: Run through envsubst
#cat "${WORKFLOW_STEPS_DIR}"/01_wf_combined.xml | envsubst > "${STATE_FOLDER}"/_wf_commands_envsubst.xml

tee "${STATE_FOLDER}"/_wf_commands.xml << EOL >/dev/null
<?xml version="1.0"?>
<?xml-stylesheet type="text/xsl" href="01_wf_combined.xsl"?>
<steps>

	<step>
		<label>Download DB Dumps</label>
		<step>
			<label>Download/Sync all in one command:</label>
			<action strip-margin="true"><![CDATA[
				|rsync --progress --partial --inplace --append-verify --compress --checksum --recursive \\
				|    -v -v \\
				|    pshdevtools.pennstatehealth.net:/archive/db_dumps/ \\
				|    ${ORACLE_DUMP_DIR_HOST_PATH}/
			]]></action>
		</step>
		<step>
			<label>OR download/sync each individually:</label>
			<action>rsync --progress --partial --inplace --append-verify --compress --checksum pshdevtools.pennstatehealth.net:/archive/db_dumps/$(getPortalVar infonet71 SRC_DMP_FILE_NAME ) ${ORACLE_DUMP_DIR_HOST_PATH}/</action>
			<action>rsync --progress --partial --inplace --append-verify --compress --checksum pshdevtools.pennstatehealth.net:/archive/db_dumps/$(getPortalVar infonet73 SRC_DMP_FILE_NAME ) ${ORACLE_DUMP_DIR_HOST_PATH}/</action>
			<action>rsync --progress --partial --inplace --append-verify --compress --checksum pshdevtools.pennstatehealth.net:/archive/db_dumps/$(getPortalVar ewp73     SRC_DMP_FILE_NAME ) ${ORACLE_DUMP_DIR_HOST_PATH}/</action>
			<action>rsync --progress --partial --inplace --append-verify --compress --checksum pshdevtools.pennstatehealth.net:/archive/db_dumps/$(getPortalVar med       SRC_DMP_FILE_NAME ) ${ORACLE_DUMP_DIR_HOST_PATH}/</action>
		</step>
	</step>

	<step>
		<label>Data Refresh Extract</label>
		<step>
			<label>Import Previous</label>
			<action type="manual">
				Import the previous Oracle VM(s) with the Schemas you are going to import.
			</action>
		</step>

		<step>
			<label>Download LDR</label>
			<action>bash ${WORKFLOW_STEPS_DIR}/global_run_with_workflow_vars.sh ${STATE_ID} ${WORKFLOW_STEPS_DIR}/run-liferay-data-refresh-get-jar.sh</action>
		</step>

		<step>
			<label>Extract Settings</label>
			<action>bash ${WORKFLOW_STEPS_DIR}/global_run_with_workflow_vars.sh ${STATE_ID} ${WORKFLOW_STEPS_DIR}/run-liferay-data-refresh-extract.sh 'infonet71'</action>
			<action>bash ${WORKFLOW_STEPS_DIR}/global_run_with_workflow_vars.sh ${STATE_ID} ${WORKFLOW_STEPS_DIR}/run-liferay-data-refresh-extract.sh 'infonet73'</action>
			<action>bash ${WORKFLOW_STEPS_DIR}/global_run_with_workflow_vars.sh ${STATE_ID} ${WORKFLOW_STEPS_DIR}/run-liferay-data-refresh-extract.sh 'ewp73'</action>
		</step>

		<action type="manual">
			Remove the Oracle VM.
		</action>
	</step>

	<step>
		<label>Provision new DB VM</label>
		<action>vagrant box update --no-tty --box 'oracle19c'</action>
		<action>bash ${WORKFLOW_STEPS_DIR}/global_run_with_workflow_vars.sh ${STATE_ID} ${WORKFLOW_STEPS_DIR}/run-provision-oracle-vagrant.sh</action>
	</step>

	<step>
		<label>Import Schemas</label>
		<step>
			<label>Connect USB Drive to VM</label>
			<action type="manual" strip-margin="true">
				|Eject S: Drive from Host OS, but do not unplug the device.
				|Open the USB Settings for '${ORACLE_VM_VAGRANT_BOX_NAME}' and add a filter for the USB Device.
			</action>
		</step>

		<step>
			<label>Mount USB</label>
			<action strip-margin="true"><![CDATA[
				|ssh -F ${STATE_FOLDER}/vagrant-ssh-${ORACLE_VM_VAGRANT_BOX_NAME} default <<- "EOL_SSH"
				|	#!/usr/bin/env bash
				|	set -e -E -u -o pipefail
				|
				|	# Check for Scratch_Space_500 drive
				|	[[  -e /dev/disk/by-label/SS_500 ]] || (echo 'Could not find USB' ; exit 9 )
				|
				|	# Make directory if none
				|	[[ -d /media/SS_500_USB ]] || sudo mkdir /media/SS_500_USB
				|
				|	# Unmount if mounted
				|	mountpoint -q /media/SS_500_USB && sudo umount /media/SS_500_USB
				|	sudo mount /dev/disk/by-label/SS_500 /media/SS_500_USB -o umask=000
				|
				|	echo "'SS_500' was mounted to /media/SS_500_USB"
				|EOL_SSH
			]]></action>
		</step>

		<step>
			<label>Import Schemas</label>
			<action>bash ${WORKFLOW_STEPS_DIR}/global_run_with_workflow_vars.sh ${STATE_ID} ${WORKFLOW_STEPS_DIR}/run-db-import.sh 'infonet71' '/media/SS_500_USB/DBDumps'</action>
			<action>bash ${WORKFLOW_STEPS_DIR}/global_run_with_workflow_vars.sh ${STATE_ID} ${WORKFLOW_STEPS_DIR}/run-db-import.sh 'infonet73' '/media/SS_500_USB/DBDumps'</action>
			<action>bash ${WORKFLOW_STEPS_DIR}/global_run_with_workflow_vars.sh ${STATE_ID} ${WORKFLOW_STEPS_DIR}/run-db-import.sh 'ewp73' '/media/SS_500_USB/DBDumps'</action>
			<action>bash ${WORKFLOW_STEPS_DIR}/global_run_with_workflow_vars.sh ${STATE_ID} ${WORKFLOW_STEPS_DIR}/run-db-import.sh 'med' '/media/SS_500_USB/DBDumps'</action>
		</step>

		<step>
			<label>Unmount USB</label>
			<action strip-margin="true"><![CDATA[
				|ssh -F ${STATE_FOLDER}/vagrant-ssh-${ORACLE_VM_VAGRANT_BOX_NAME} default <<- "EOL_SSH"
				|	sudo umount /media/SS_500_USB
				|	sudo rm -rf /media/SS_500_USB
				|EOL_SSH
			]]></action>
		</step>

		<step>
			<label>Data Refresh Load</label>
			<action>bash ${WORKFLOW_STEPS_DIR}/global_run_with_workflow_vars.sh ${STATE_ID} ${WORKFLOW_STEPS_DIR}/run-liferay-data-refresh-load.sh 'infonet71'</action>
			<action>bash ${WORKFLOW_STEPS_DIR}/global_run_with_workflow_vars.sh ${STATE_ID} ${WORKFLOW_STEPS_DIR}/run-liferay-data-refresh-load.sh 'infonet73'</action>
			<action>bash ${WORKFLOW_STEPS_DIR}/global_run_with_workflow_vars.sh ${STATE_ID} ${WORKFLOW_STEPS_DIR}/run-liferay-data-refresh-load.sh 'ewp73'</action>
		</step>

		<step>
			<label>Apply LOCAL-specific Data Transformations</label>
			<action>bash ${WORKFLOW_STEPS_DIR}/global_run_with_workflow_vars.sh ${STATE_ID} ${WORKFLOW_STEPS_DIR}/run-local-sql-transformations.sh 'infonet71'</action>
			<action>bash ${WORKFLOW_STEPS_DIR}/global_run_with_workflow_vars.sh ${STATE_ID} ${WORKFLOW_STEPS_DIR}/run-local-sql-transformations.sh 'infonet73'</action>
			<action>bash ${WORKFLOW_STEPS_DIR}/global_run_with_workflow_vars.sh ${STATE_ID} ${WORKFLOW_STEPS_DIR}/run-local-sql-transformations.sh 'ewp73'</action>
		</step>

		<step>
			<label>Snapshot</label>
			<action>( cd ${STATE_FOLDER}/vagrant/${ORACLE_VM_VAGRANT_BOX_NAME} ; vagrant snapshot save 'before-vagrant' )</action>
		</step>
	</step>

	<step>
		<label>Download Latest Base Boxes</label>
		<action>vagrant box update --no-tty --box 'liferay71'</action>
		<action>vagrant box update --no-tty --box 'liferay73'</action>
	</step>

	<step>
		<label>Provision Vagrant Boxes</label>
		<action>bash ${WORKFLOW_STEPS_DIR}/global_run_with_workflow_vars.sh ${STATE_ID} ${WORKFLOW_STEPS_DIR}/run-provision-liferay71-portal.sh 'infonet71' 'infonet71-base'</action>
		<action>bash ${WORKFLOW_STEPS_DIR}/global_run_with_workflow_vars.sh ${STATE_ID} ${WORKFLOW_STEPS_DIR}/run-provision-liferay73-portal.sh 'infonet73' 'infonet73-base'</action>
		<action>bash ${WORKFLOW_STEPS_DIR}/global_run_with_workflow_vars.sh ${STATE_ID} ${WORKFLOW_STEPS_DIR}/run-provision-liferay73-portal.sh 'ewp73' 'ewp73-base'</action>
	</step>

	<step>
		<label>View Logs (Run this in a new window)</label>
		<action>bash ${WORKFLOW_STEPS_DIR}/global_run_with_workflow_vars.sh ${STATE_ID} ${WORKFLOW_STEPS_DIR}/run-liferay-log.sh 'infonet71-base'</action>
		<action>bash ${WORKFLOW_STEPS_DIR}/global_run_with_workflow_vars.sh ${STATE_ID} ${WORKFLOW_STEPS_DIR}/run-liferay-log.sh 'infonet73-base'</action>
		<action>bash ${WORKFLOW_STEPS_DIR}/global_run_with_workflow_vars.sh ${STATE_ID} ${WORKFLOW_STEPS_DIR}/run-liferay-log.sh 'ewp73-base'</action>
	</step>

	<step>
		<label>Start Liferay</label>
		<action>bash ${WORKFLOW_STEPS_DIR}/global_run_with_workflow_vars.sh ${STATE_ID} ${WORKFLOW_STEPS_DIR}/run-liferay-restart.sh 'infonet71-base'</action>
		<action>bash ${WORKFLOW_STEPS_DIR}/global_run_with_workflow_vars.sh ${STATE_ID} ${WORKFLOW_STEPS_DIR}/run-liferay-restart.sh 'infonet73-base'</action>
		<action>bash ${WORKFLOW_STEPS_DIR}/global_run_with_workflow_vars.sh ${STATE_ID} ${WORKFLOW_STEPS_DIR}/run-liferay-restart.sh 'ewp73-base'</action>
	</step>

	<step>
		<label>Initial Deploy of PSH Plugins</label>
		<action>bash ${WORKFLOW_STEPS_DIR}/global_run_with_workflow_vars.sh ${STATE_ID} ${WORKFLOW_STEPS_DIR}/run-deploy-plugins.sh 'infonet71' 'infonet71-base'</action>
		<action>bash ${WORKFLOW_STEPS_DIR}/global_run_with_workflow_vars.sh ${STATE_ID} ${WORKFLOW_STEPS_DIR}/run-deploy-plugins.sh 'infonet73' 'infonet73-base'</action>
		<action>bash ${WORKFLOW_STEPS_DIR}/global_run_with_workflow_vars.sh ${STATE_ID} ${WORKFLOW_STEPS_DIR}/run-deploy-plugins.sh 'ewp73' 'ewp73-base'</action>
	</step>

	<step>
		<label>Restart the server</label>
		<action>bash ${WORKFLOW_STEPS_DIR}/global_run_with_workflow_vars.sh ${STATE_ID} ${WORKFLOW_STEPS_DIR}/run-liferay-restart.sh 'infonet71-base'</action>
		<action>bash ${WORKFLOW_STEPS_DIR}/global_run_with_workflow_vars.sh ${STATE_ID} ${WORKFLOW_STEPS_DIR}/run-liferay-restart.sh 'infonet73-base'</action>
		<action>bash ${WORKFLOW_STEPS_DIR}/global_run_with_workflow_vars.sh ${STATE_ID} ${WORKFLOW_STEPS_DIR}/run-liferay-restart.sh 'ewp73-base'</action>
	</step>

	<step>
		<label>Spot Check that server is running</label>
		<action type="manual" strip-margin="true">
			|Open "https://infonet-local:50300/group/control_panel/manage/-/server/external-services" in a web browser.
			|Spot check that the server is working and returning expected Prod data. Don't create any resources.
		</action>
		<action type="manual" strip-margin="true">
			|Open "https://infonet3-local:50600/group/control_panel/manage?p_p_id=com_liferay_server_admin_web_portlet_ServerAdminPortlet" in a web browser.
			|Spot check that the server is working and returning expected Prod data. Don't create any resources.
		</action>
		<action type="manual" strip-margin="true">
			|Open "https://cancer-local:50400/group/control_panel/manage?p_p_id=com_liferay_server_admin_web_portlet_ServerAdminPortlet" in a web browser.
			|Spot check that the server is working and returning expected Prod data. Don't create any resources.
		</action>
	</step>

	<step>
		<label>Data Reduction</label>
		<action type="manual" strip-margin="true">
			|Open "https://infonet-local:50300/group/control_panel/manage?p_p_id=com_liferay_server_admin_web_portlet_ServerAdminPortlet" in a web browser
			|'Reset preview and thumbnail files for Documents and Media portlet.' -> Execute
		</action>
		<action type="manual" strip-margin="true">
			|Open "https://infonet3-local:50600/group/control_panel/manage?p_p_id=com_liferay_server_admin_web_portlet_ServerAdminPortlet" in a web browser
			|'Reset preview and thumbnail files for Documents and Media portlet.' -> Execute
		</action>
		<action type="manual" strip-margin="true">
			|Open "https://cancer-local:50400/group/control_panel/manage?p_p_id=com_liferay_server_admin_web_portlet_ServerAdminPortlet" in a web browser
			|'Reset preview and thumbnail files for Documents and Media portlet.' -> Execute
		</action>
	</step>

	<step>
		<label>ElasticSearch reindex</label>
		<action type="manual" strip-margin="true">
			|Open "https://infonet-local:50300/group/control_panel/manage?p_p_id=com_liferay_portal_search_admin_web_portlet_SearchAdminPortlet" in a web browser
			|'Reindex all search indexes.' -> Execute
		</action>
		<action type="manual" strip-margin="true">
			|Open "https://infonet3-local:50600/group/control_panel/manage?p_p_id=com_liferay_portal_search_admin_web_portlet_SearchAdminPortlet&amp;_com_liferay_portal_search_admin_web_portlet_SearchAdminPortlet_tabs1=index-actions" in a web browser
			|'Reindex all search indexes.' -> Execute
		</action>
		<action type="manual" strip-margin="true">
			|Open "https://cancer-local:50400/group/control_panel/manage?p_p_id=com_liferay_portal_search_admin_web_portlet_SearchAdminPortlet&amp;_com_liferay_portal_search_admin_web_portlet_SearchAdminPortlet_tabs1=index-actions" in a web browser
			|'Reindex all search indexes.' -> Execute
		</action>
	</step>

	<step>
		<label>Shutdown</label>
		<action>bash ${WORKFLOW_STEPS_DIR}/global_run_with_workflow_vars.sh ${STATE_ID} ${WORKFLOW_STEPS_DIR}/run-liferay-shutdown.sh 'infonet71-base'</action>
		<action>bash ${WORKFLOW_STEPS_DIR}/global_run_with_workflow_vars.sh ${STATE_ID} ${WORKFLOW_STEPS_DIR}/run-liferay-shutdown.sh 'infonet73-base'</action>
		<action>bash ${WORKFLOW_STEPS_DIR}/global_run_with_workflow_vars.sh ${STATE_ID} ${WORKFLOW_STEPS_DIR}/run-liferay-shutdown.sh 'ewp73-base'</action>
	</step>

	<step>
		<label>Start Liferay Service On OS Startup</label>
		<action>bash ${WORKFLOW_STEPS_DIR}/global_run_with_workflow_vars.sh ${STATE_ID} ${WORKFLOW_STEPS_DIR}/run-enable-liferay-service.sh 'infonet71-base'</action>
		<action>bash ${WORKFLOW_STEPS_DIR}/global_run_with_workflow_vars.sh ${STATE_ID} ${WORKFLOW_STEPS_DIR}/run-enable-liferay-service.sh 'infonet73-base'</action>
		<action>bash ${WORKFLOW_STEPS_DIR}/global_run_with_workflow_vars.sh ${STATE_ID} ${WORKFLOW_STEPS_DIR}/run-enable-liferay-service.sh 'ewp73-base'</action>
	</step>

	<step>
		<label>Revert the JVM Memory settings back to their default</label>
		<action>bash ${WORKFLOW_STEPS_DIR}/global_run_with_workflow_vars.sh ${STATE_ID} ${WORKFLOW_STEPS_DIR}/run-configure-jvm-memory-settings.sh 'infonet71-base' '2g'</action>
		<action>bash ${WORKFLOW_STEPS_DIR}/global_run_with_workflow_vars.sh ${STATE_ID} ${WORKFLOW_STEPS_DIR}/run-configure-jvm-memory-settings.sh 'infonet73-base' '2g'</action>
		<action>bash ${WORKFLOW_STEPS_DIR}/global_run_with_workflow_vars.sh ${STATE_ID} ${WORKFLOW_STEPS_DIR}/run-configure-jvm-memory-settings.sh 'ewp73-base' '2g'</action>
	</step>

	<step>
		<label>Extract VM and Liferay Info</label>
		<action>bash ${WORKFLOW_STEPS_DIR}/global_run_with_workflow_vars.sh ${STATE_ID} ${WORKFLOW_STEPS_DIR}/run-extract-vm-info.sh 'infonet71' 'infonet71-base'</action>
		<action>bash ${WORKFLOW_STEPS_DIR}/global_run_with_workflow_vars.sh ${STATE_ID} ${WORKFLOW_STEPS_DIR}/run-extract-vm-info.sh 'infonet73' 'infonet73-base'</action>
		<action>bash ${WORKFLOW_STEPS_DIR}/global_run_with_workflow_vars.sh ${STATE_ID} ${WORKFLOW_STEPS_DIR}/run-extract-vm-info.sh 'ewp73' 'ewp73-base'</action>
	</step>

	<step>
		<label>Update Vagrantfiles</label>
		<action type="manual" strip-margin="true">
			|Update Vagrantfiles to reference the correct AppDev_ORCL version.
			|Thought: A script that scans the Vagrantfiles for the ORCL Version, and gives a warning.
		</action>
	</step>

	<step>
		<label>Prepare Box for Packaging</label>
		<action>bash ${WORKFLOW_STEPS_DIR}/global_run_with_workflow_vars.sh ${STATE_ID} ${WORKFLOW_STEPS_DIR}/run-vagrant-compress.sh 'infonet71-base'</action>
		<action>bash ${WORKFLOW_STEPS_DIR}/global_run_with_workflow_vars.sh ${STATE_ID} ${WORKFLOW_STEPS_DIR}/run-vagrant-compress.sh 'infonet73-base'</action>
		<action>bash ${WORKFLOW_STEPS_DIR}/global_run_with_workflow_vars.sh ${STATE_ID} ${WORKFLOW_STEPS_DIR}/run-vagrant-compress.sh 'ewp73-base'</action>
	</step>

	<step>
		<label>Package</label>
		<action>bash ${WORKFLOW_STEPS_DIR}/global_run_with_workflow_vars.sh ${STATE_ID} ${WORKFLOW_STEPS_DIR}/run-package-box.sh 'infonet71' 'infonet71-base'</action>
		<action>bash ${WORKFLOW_STEPS_DIR}/global_run_with_workflow_vars.sh ${STATE_ID} ${WORKFLOW_STEPS_DIR}/run-package-box.sh 'infonet73' 'infonet73-base'</action>
		<action>bash ${WORKFLOW_STEPS_DIR}/global_run_with_workflow_vars.sh ${STATE_ID} ${WORKFLOW_STEPS_DIR}/run-package-box.sh 'ewp73' 'ewp73-base'</action>
	</step>

	<step>
		<label>Compress Database VM</label>

		<step>
			<label>Snapshot</label>
			<action>( cd ${STATE_FOLDER}/vagrant/${ORACLE_VM_VAGRANT_BOX_NAME} ;     vagrant snapshot save 'before-compress' )</action>
		</step>

		<step>
			<label>Mount USB</label>
			<action strip-margin="true"><![CDATA[
				|ssh -F ${STATE_FOLDER}/vagrant-ssh-${ORACLE_VM_VAGRANT_BOX_NAME} default <<- "EOL_SSH"
				|	#!/usr/bin/env bash
				|	set -e -E -u -o pipefail
				|
				|	# Check for Scratch_Space_500 drive
				|	[[  -e /dev/disk/by-label/SS_500 ]] || (echo 'Could not find USB' ; exit 9 )
				|
				|	# Make directory if none
				|	[[ -d /media/SS_500_USB ]] || sudo mkdir /media/SS_500_USB
				|
				|	# Unmount if mounted
				|	mountpoint -q /media/SS_500_USB && sudo umount /media/SS_500_USB
				|	sudo mount /dev/disk/by-label/SS_500 /media/SS_500_USB -o umask=000
				|
				|	echo "'SS_500' was mounted to /media/SS_500_USB"
				|EOL_SSH
			]]></action>
		</step>

		<step>
			<label>Reduce Disk Footprint of Tablespaces</label>
			<action>bash ${WORKFLOW_STEPS_DIR}/global_run_with_workflow_vars.sh ${STATE_ID} ${WORKFLOW_STEPS_DIR}/run-db-tablespace-compress.sh 'infonet71' '/media/SS_500_USB/DBDumps'</action>
			<action>bash ${WORKFLOW_STEPS_DIR}/global_run_with_workflow_vars.sh ${STATE_ID} ${WORKFLOW_STEPS_DIR}/run-db-tablespace-compress.sh 'infonet73' '/media/SS_500_USB/DBDumps'</action>
			<action>bash ${WORKFLOW_STEPS_DIR}/global_run_with_workflow_vars.sh ${STATE_ID} ${WORKFLOW_STEPS_DIR}/run-db-tablespace-compress.sh 'ewp73' '/media/SS_500_USB/DBDumps'</action>
		</step>

		<step>
			<label>Unmount USB</label>
			<action strip-margin="true"><![CDATA[
				|ssh -F ${STATE_FOLDER}/vagrant-ssh-${ORACLE_VM_VAGRANT_BOX_NAME} default <<- EOL_SSH
				|	sudo umount /media/SS_500_USB
				|	sudo rm -rf /media/SS_500_USB
				|EOL_SSH
			]]></action>
			<action type="manual">
				Remove USB Filter from VM Guest settings, unplug and replugin USB Drive, and check that the S: Drive is mounted in the Host
			</action>
		</step>
	</step>

	<step>
		<label>Export Database VM</label>
		<action>( cd ${STATE_FOLDER}/vagrant/${ORACLE_VM_VAGRANT_BOX_NAME} ; vagrant snapshot save 'before-export' )</action>
		<action>bash ${WORKFLOW_STEPS_DIR}/global_run_with_workflow_vars.sh ${STATE_ID} ${WORKFLOW_STEPS_DIR}/run-export-oracle-vagrant-as-vm.sh '${ORACLE_VM_EXPORTED_VBOX_NAME}'</action>
	</step>

	<step>
		<action type="manual">Import the recently created OVA, and start in headless mode</action>
	</step>

	<step>
		<label>Verify Packaged Boxes</label>
		<step>
			<label>Add Packaged Boxes</label>
			<action>vagrant box add --name 'infonet71-verify' --force '$( toOSPath "${STATE_FOLDER}"/vagrant/infonet71.box )'</action>
			<action>vagrant box add --name 'infonet73-verify' --force '$( toOSPath "${STATE_FOLDER}"/vagrant/infonet73.box )'</action>
			<action>vagrant box add --name 'ewp73-verify' --force '$( toOSPath "${STATE_FOLDER}"/vagrant/ewp73.box )'</action>
		</step>

		<step>
			<label>Launch VMs</label>
			<action>bash ${WORKFLOW_STEPS_DIR}/global_run_with_workflow_vars.sh ${STATE_ID} ${WORKFLOW_STEPS_DIR}/run-launch-new-box.sh 'infonet71' 'infonet71-verify'</action>
			<action>bash ${WORKFLOW_STEPS_DIR}/global_run_with_workflow_vars.sh ${STATE_ID} ${WORKFLOW_STEPS_DIR}/run-launch-new-box.sh 'infonet73' 'infonet73-verify'</action>
			<action>bash ${WORKFLOW_STEPS_DIR}/global_run_with_workflow_vars.sh ${STATE_ID} ${WORKFLOW_STEPS_DIR}/run-launch-new-box.sh 'ewp73' 'ewp73-verify'</action>
		</step>

		<step>
			<label>Spot Check Packaged Vagrant Boxes</label>
			<action>bash ${WORKFLOW_STEPS_DIR}/global_run_with_workflow_vars.sh ${STATE_ID} ${WORKFLOW_STEPS_DIR}/run-liferay-log.sh 'infonet71-verify'</action>
			<action>bash ${WORKFLOW_STEPS_DIR}/global_run_with_workflow_vars.sh ${STATE_ID} ${WORKFLOW_STEPS_DIR}/run-liferay-log.sh 'infonet73-verify'</action>
			<action>bash ${WORKFLOW_STEPS_DIR}/global_run_with_workflow_vars.sh ${STATE_ID} ${WORKFLOW_STEPS_DIR}/run-liferay-log.sh 'ewp73-verify'</action>
		</step>

		<step>
			<label>If errors are found, rollback the Base VMs and fix</label>
			<step>
				<action type="manual">Restore the VMs to before "Prepare Box for Packaging" step (run-vagrant-compress.sh)</action>
				<step>
					<action>( cd ${STATE_FOLDER}/vagrant/infonet71-base ;          vagrant snapshot restore 'before-compress' )</action>
					<action>( cd ${STATE_FOLDER}/vagrant/infonet73-base ;          vagrant snapshot restore 'before-compress' )</action>
					<action>( cd ${STATE_FOLDER}/vagrant/ewp73-base ;              vagrant snapshot restore 'before-compress' )</action>
					<action>( cd ${STATE_FOLDER}/vagrant/${ORACLE_VM_VAGRANT_BOX_NAME} ; vagrant snapshot restore 'before-compress' )</action>
				</step>
				<action type="manual">Fix the issues</action>
				<action type="manual">Perform all steps on the erroneous VMs starting with "Prepare Box for Packaging"</action>
			</step>
		</step>
		<step>
			<label>Halt verify Vagrants</label>
			<action>bash ${WORKFLOW_STEPS_DIR}/global_run_with_workflow_vars.sh ${STATE_ID} ${WORKFLOW_STEPS_DIR}/run-vagrant-halt.sh 'infonet71-verify'</action>
			<action>bash ${WORKFLOW_STEPS_DIR}/global_run_with_workflow_vars.sh ${STATE_ID} ${WORKFLOW_STEPS_DIR}/run-vagrant-halt.sh 'infonet73-verify'</action>
			<action>bash ${WORKFLOW_STEPS_DIR}/global_run_with_workflow_vars.sh ${STATE_ID} ${WORKFLOW_STEPS_DIR}/run-vagrant-halt.sh 'ewp73-verify'</action>
		</step>
	</step>

	<step>
		<label>Publish</label>
		<step>
			<label>Upload Oracle OVA to pshdevtools</label>
			<action strip-margin="true">
				|rsync --progress --partial --inplace --append-verify --compress --checksum --perms --chmod='g=rw' \\
				|  ${VBOX_EXPORT_PATH}/${ORACLE_VM_EXPORTED_VBOX_NAME}.ova pshdevtools.pennstatehealth.net:/archive/vagrant_boxes/
			</action>
		</step>

		<step>
			<label>Upload Vagrant Boxes to Artifactory</label>
			<action>bash ${WORKFLOW_STEPS_DIR}/global_run_with_workflow_vars.sh ${STATE_ID} ${WORKFLOW_STEPS_DIR}/run-push-to-artifactory.sh 'infonet71'</action>
			<action>bash ${WORKFLOW_STEPS_DIR}/global_run_with_workflow_vars.sh ${STATE_ID} ${WORKFLOW_STEPS_DIR}/run-push-to-artifactory.sh 'infonet73'</action>
			<action>bash ${WORKFLOW_STEPS_DIR}/global_run_with_workflow_vars.sh ${STATE_ID} ${WORKFLOW_STEPS_DIR}/run-push-to-artifactory.sh 'ewp73'</action>
		</step>
	</step>

	<step>
		<label>Clean Up</label>

		<step>
			<label>Destroy Vagrant VMs</label>
			<action strip-margin="true">
				|bash ${WORKFLOW_STEPS_DIR}/global_run_with_workflow_vars.sh ${STATE_ID} ${WORKFLOW_STEPS_DIR}/run-vagrant-destroy-remove.sh \\
				|	'${ORACLE_VM_VAGRANT_BOX_NAME}' \\
				|	'infonet71-verify' \\
				|	'infonet71-base' \\
				|	'infonet73-verify' \\
				|	'infonet73-base' \\
				|	'ewp73-verify' \\
				|	'ewp73-base'
			</action>
		</step>

		<step>
			<label>Remove .box files</label>
			<action>rm ${STATE_FOLDER}/vagrant/infonet71.box</action>
			<action>rm ${STATE_FOLDER}/vagrant/infonet73.box</action>
			<action>rm ${STATE_FOLDER}/vagrant/ewp73.box</action>
		</step>

		<step>
			<action type="manual">Remove older archived dmp files</action>
		</step>

		<step>
			<label>Compress dmp files</label>
			<action>bash ${WORKFLOW_STEPS_DIR}/global_run_with_workflow_vars.sh ${STATE_ID} ${WORKFLOW_STEPS_DIR}/run-compress-dmp-file.sh 'infonet71'</action>
			<action>bash ${WORKFLOW_STEPS_DIR}/global_run_with_workflow_vars.sh ${STATE_ID} ${WORKFLOW_STEPS_DIR}/run-compress-dmp-file.sh 'infonet73'</action>
			<action>bash ${WORKFLOW_STEPS_DIR}/global_run_with_workflow_vars.sh ${STATE_ID} ${WORKFLOW_STEPS_DIR}/run-compress-dmp-file.sh 'ewp73'</action>
			<action>bash ${WORKFLOW_STEPS_DIR}/global_run_with_workflow_vars.sh ${STATE_ID} ${WORKFLOW_STEPS_DIR}/run-compress-dmp-file.sh 'med'</action>
		</step>
	</step>
</steps>
EOL


bash "${SCRIPTS_DIR}"/saxon/transform_xml.sh \
    "${STATE_FOLDER}"/_wf_commands.xml \
    "${WORKFLOW_STEPS_DIR}"/01_wf_combined.xsl \
    "${STATE_FOLDER}"/_wf_commands.html

echo 'Workflow started.'
echo "Workflow Id: ${STATE_ID}"
commands_html_path="$( toOSPath "${STATE_FOLDER}"/_wf_commands.html )"

echo "Commands at: file:///${commands_html_path//\///}"

exit 0
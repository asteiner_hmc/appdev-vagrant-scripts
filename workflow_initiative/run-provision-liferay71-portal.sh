#!/usr/bin/env bash

###################################################################
#
#    Run the scripts for building a Portals Vagrant based on the liferay71 Vagrant.
#
#    Parameters
#        1    portal_name (infonet71)
#
#    IMPORTANT:
#        This script is not meant to be run directly, but as an argument passed
#        in via global_run_with_workflow_vars.sh
###################################################################

set -e -E -u -o pipefail

# Uncomment to see commands that fail.
# trap 'echo "Error at ${BASH_SOURCE[0]}:$LINENO. Command returned $? : $BASH_COMMAND" ; exit 255' ERR

case "$(uname -sr)" in
    Darwin*)
        # Mac OS X
        #For OSX, use coreutils if installed
        PATH="/usr/local/opt/coreutils/libexec/gnubin:$PATH"
        ;;

    CYGWIN*|MINGW*|MINGW32*|MSYS*)
        # CYGWIN on Windows
        ;;

    # Future Use
    Linux*Microsoft*)
        ;&
    Linux*)
        ;&
    *)
        # Other
        echo "OS $(uname -sr) is not supported by this script"
        exit 255
        ;;
esac

portal_name="$1"

new_box_name="$2"

# Bring in vars
source "$STATE_FOLDER"/vars-global.sh "${portal_name}"

new_vagrant_dir="${STATE_FOLDER}"/vagrant/"${new_box_name}"

mkdir -p "${new_vagrant_dir}"

bash "${SCRIPTS_DIR}"/liferay/liferay71-portal-provisioning/build-liferay71-portal.sh \
  "${new_vagrant_dir}" \
  7168 \
  '5g' \
  "${BOX_VAGRANT_FILE}"


echo 'Saving SSH info to file'
( cd "${new_vagrant_dir}" ; vagrant ssh-config > "${new_vagrant_dir}"/vagrant-ssh-config )

# Extract the Host key Ex: Host: oracle-19c-vagrant and save to file (Future utilization)
ssh_host_key="$(perl -ne 'exit if /^Host (\S*)\R?$/ && print "$1\n"' ${new_vagrant_dir}/vagrant-ssh-config)"
echo "${ssh_host_key}" > "${new_vagrant_dir}"/vagrant-ssh-host

# Legacy location
cp "${new_vagrant_dir}"/vagrant-ssh-config "${STATE_FOLDER}"/vagrant-ssh-"${new_box_name}"


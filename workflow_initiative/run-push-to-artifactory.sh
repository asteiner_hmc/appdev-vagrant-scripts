#!/usr/bin/env bash

###################################################################
#
#    Parameters
#        1    PORTAL_NAME (ewp73, infonet71, infonet73)
#
#    Variables expected to be set:
#        STATE_FOLDER    (ex: /Users/asteiner/Dev/data/Ec4.8-g2.4/local-env-update-scripts/package-vagrants/infonet62_Vagrantfile )
#
#    IMPORTANT:
#        This script is not meant to be run directly, but as an argument passed
#        in via global_run_with_workflow_vars.sh
###################################################################

set -e -E -u -o pipefail

PORTAL_NAME="$1"

source "${STATE_FOLDER}"/vars-global.sh "${PORTAL_NAME}"
BOX_OS_KERNEL_VERSION="BOX_OS_KERNEL_VERSION_${PORTAL_NAME^^}"
BOX_OS_KERNEL_VERSION="${!BOX_OS_KERNEL_VERSION}"

BOX_OS_RELEASE_VERSION="BOX_OS_RELEASE_VERSION_${PORTAL_NAME^^}"
BOX_OS_RELEASE_VERSION="${!BOX_OS_RELEASE_VERSION}"

properties=(
	'kernel.name'              'Linux'
	'vagrant.version'          "${BOX_VAGRANT_VERSION}"
	'vagrant.provider_version' "${BOX_VIRTUALBOX_VERSION}"
	'os.family'                'RedHat'
	'os.name'                  'RedHat Enterprise Linux Server'
	'os.architecture'          'x86_64'
	'kernel.release'           "${BOX_OS_KERNEL_VERSION}"
	'os.release_full'          "${BOX_OS_RELEASE_VERSION}"
	'liferay.platform_version' "${BOX_LIFERAY_PLATFORM_VERSION}"
)

# Optional Properties
set +u
[[ -n "${BOX_LIFERAY_FIXPACK_VERSION}" ]] && properties+=(
	'liferay.fixpack_version' "${BOX_LIFERAY_FIXPACK_VERSION}"
)
[[ -n "${BOX_LIFERAY_HOTFIX_VERSION}" ]] && properties+=(
	'liferay.hotfix_version' "${BOX_LIFERAY_HOTFIX_VERSION}"
)
[[ -n "${PORTAL_CORE_RELEASE_VERSION}" ]] && properties+=(
	'liferay.portals_core_version' "${PORTAL_CORE_RELEASE_VERSION}"
)
[[ -n "${PORTAL_THIS_RELEASE_VERSION}" && -n "${CONFIG_THIS_ENV}" ]] && properties+=(
	"liferay.portals_${CONFIG_THIS_ENV}_version" "${PORTAL_THIS_RELEASE_VERSION}"
)	
set -u
#printf -v properties %s "${properties[@]}" # Concatenate properties array into string

"${SCRIPTS_DIR}"/publish-to-artifactory.sh "${BOX_NAME}" \
  "${STATE_FOLDER}/vagrant/${BOX_NAME}.box" \
  "${BOX_VERSION}" \
  'https://artifact.pennstatehealth.net/artifactory' \
  vagrant-pshdev-local \
  \
  "${properties[@]}"

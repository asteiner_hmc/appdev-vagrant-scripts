#!/usr/bin/env bash
###################################################################
#
#    Exports the schema and reimports it, to clear out Oracle and system cache files
#
#    Parameters
#        1    portal_name (ewp62, infonet71)
#        2    Folder on Oracle VM to store the temporary dump file.  To keep the VM's FS small, this should be on a mounted folder
#
#    IMPORTANT:
#        This script is not meant to be run directly, but as an argument passed
#        in via global_run_with_workflow_vars.sh
###################################################################

set -e -E -u -o pipefail

portal_name="$1"

source "$STATE_FOLDER"/vars-global.sh "${portal_name}"

ORACLE_VM_DUMPS_FOLDER="$2"


bash "${SCRIPTS_DIR}"/run-remote-script.sh \
  --ssh-config-file "${STATE_FOLDER}/vagrant-ssh-${ORACLE_VM_VAGRANT_BOX_NAME}" \
  --run-as "${ORACLE_ORCL_USER_USER}" \
  --run-as-with-login-shell \
  -- \
  "${SCRIPTS_DIR}"/oracle/export-schema-19c.sh \
  --job-name "${CONFIG_ORACLE_SCHEMA_USER}_compress_export" \
  --schema "${CONFIG_ORACLE_SCHEMA_USER}" \
  --sid "${ORACLE_ORCL_SID}" \
  --system-pass "${ORACLE_ORCL_SYSTEM_PASS}" \
  --dump-file "${ORACLE_VM_DUMPS_FOLDER}/${CONFIG_ORACLE_SCHEMA_USER}_compress%L.dmp" \
  --par-extras $'filesize=500mb'

# The --run-as-with-login-shell flag is VERY IMPORTANT.  Not using it causes unexpected behaviors when
# running CREATE TABLESPACE and CREATE USER commands in sqlcl.
# (Things like "ORA-65096: invalid common user or role name", "ORA-01157: cannot identify/lock data file 4 - see DBWR trace file", and "ORA-01110: data file 4: '/u01/app/oracle/orcl/users01.dbf'")

bash "${SCRIPTS_DIR}"/run-remote-script.sh \
  --ssh-config-file "${STATE_FOLDER}/vagrant-ssh-${ORACLE_VM_VAGRANT_BOX_NAME}" \
  --run-as "${ORACLE_ORCL_USER_USER}" \
  --run-as-with-login-shell \
  -- \
  "${SCRIPTS_DIR}"/oracle/import-schema-19c.sh \
  --job-name "${CONFIG_ORACLE_SCHEMA_USER}_import_compress" \
  --dump-file "${ORACLE_VM_DUMPS_FOLDER}/${CONFIG_ORACLE_SCHEMA_USER}_compress%L.dmp" \
  --src-schema "${CONFIG_ORACLE_SCHEMA_USER}" \
  --src-tablespace "${CONFIG_ORACLE_SCHEMA_TABLESPACE}" \
  --dest-schema "${CONFIG_ORACLE_SCHEMA_USER}" \
  --dest-schema-password "${CONFIG_ORACLE_SCHEMA_PASS}" \
  --dest-tablespace "${CONFIG_ORACLE_SCHEMA_TABLESPACE}" \
  --sid "${ORACLE_ORCL_SID}" \
  --system-pass "${ORACLE_ORCL_SYSTEM_PASS}" \
  --tablespace-dir "${ORACLE_BIG_TABLESPACE_PATH}"

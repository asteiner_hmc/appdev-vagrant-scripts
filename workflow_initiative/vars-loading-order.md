# Expected Loading Order Of Variables  
*Scripts have access to the variables declared in scripts above them.*
*Scripts do not have access to the variables declared in scripts beneath them.*

## global_run_with_workflow_vars.sh
* STATE_FOLDER
* Variables in STATE_FOLDER (A variable is exported for each file starting with "VAR_", where the text after "VAR_" is the name, and the file's contents is the value.)
* vars-global.sh
* SCRIPTS_DIR
* global_workflow_functions.sh

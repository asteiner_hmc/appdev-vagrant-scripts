#!/usr/bin/env bash

###################################################################
#
#    Configures, Prepares, and Exports the Oracle Vagrant as a normal VirtualBox VM.
#
#    Parameters
#        1    NEW_VM_NAME    Name to export the Oracle VM as.
#
#    Outputs
#        An OVA file of the Oracle VM.
#
#    IMPORTANT:
#        This script is not meant to be run directly, but as an argument passed
#        in via global_run_with_workflow_vars.sh
###################################################################

set -e -E -u -o pipefail

new_vm_name="$1"

source "$STATE_FOLDER"/vars-global.sh ''

bash "$SCRIPTS_DIR"/run-remote-script.sh \
  --ssh-config-file "${STATE_FOLDER}"/vagrant-ssh-"${ORACLE_VM_VAGRANT_BOX_NAME}" \
  -- \
  "${SCRIPTS_DIR}"/enable-vagrant-user-login.sh "${ORACLE_ORCL_USER_USER}"

# Unmount all Shared Folders, and remove auto-mount entries
ssh -F "${STATE_FOLDER}"/vagrant-ssh-"${ORACLE_VM_VAGRANT_BOX_NAME}" default <<- "EOL"
	sudo umount --all --types vboxsf
	sudo sed -r '/\svboxsf\s/d' -i /etc/fstab
EOL

bash "$SCRIPTS_DIR"/run-remote-script.sh \
  --ssh-config-file "${STATE_FOLDER}"/vagrant-ssh-"${ORACLE_VM_VAGRANT_BOX_NAME}" \
  --run-as 'root' \
  -- \
  "${SCRIPTS_DIR}"/oracle/oracle19c-provisioning/guest/prep-oracle-vagrant-for-packaging.sh

bash "${SCRIPTS_DIR}"/shutdown-vm.sh "${ORACLE_VM_VAGRANT_BOX_NAME}"

# Remove Configured Shared Folders from VM
mapfile -t -d $'\0' folders  < <(vboxmanage showvminfo "${ORACLE_VM_VAGRANT_BOX_NAME}" --machinereadable | perl -ne '/^SharedFolderName.*?="(\w+)"/ && print "$1\0";')
for sf_name in "${folders[@]}"; do
	echo "${sf_name}"
	VBoxManage sharedfolder remove "${ORACLE_VM_VAGRANT_BOX_NAME}" --name "${sf_name}"
done


bash "${SCRIPTS_DIR}"/export-vm.sh "${ORACLE_VM_VAGRANT_BOX_NAME}" "${new_vm_name}" "${VBOX_EXPORT_PATH}/${new_vm_name}.ova"
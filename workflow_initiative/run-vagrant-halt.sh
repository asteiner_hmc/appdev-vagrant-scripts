#!/usr/bin/env bash

###################################################################
#
#    Halts Vagrant boxes in $STATE_FOLDER/vagrant
#
#    Parameters
#        1..N    vagrant_box_name (ewp62, ewp62-base, etc)
#
#    IMPORTANT:
#        This script is not meant to be run directly, but as an argument passed
#        in via global_run_with_workflow_vars.sh
###################################################################

set -e -E -u -o pipefail

if [ "$#" -lt 1 ]; then
	echo  "Please call with at least one argument"
	exit
else
	echo -e "\c"
fi

for vagrant_box_name in "$@"
do
	echo "Halting Vagrant at $STATE_FOLDER/vagrant/${vagrant_box_name}"
	cd "$STATE_FOLDER/vagrant/${vagrant_box_name}"
	vagrant halt
done


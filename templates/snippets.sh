
# Bash Cheat Sheet:  https://devhints.io/bash


# Heredoc to File
## TABS ARE IMPORTANT!
tee '.a_file' <<- EOL > /dev/null
	include_vagrantfile = File.expand_path("../Vagrantfile-base", __FILE__)
	load include_vagrantfile if File.exist?(include_vagrantfile)
	
	Vagrant.configure("2") do |config|
	  config.vm.box = "liferay73"
	end
EOL

# Heredoc to SSH
## TABS ARE IMPORTANT!
ssh -F vagrant-ssh-config "${ssh_host_key}" bash <<- EOL_SSH
	set -e -E -u -o pipefail
EOL_SSH


# Store associative array to String, then deserialize
CONFIG_REPOS="['liferay-internet-v3']='one' ['liferay-tomcat-internet-v3']='two'"
eval "declare -A config_repos_arr=(${CONFIG_REPOS})"
## Test
[[ "${config_repos_arr[liferay-internet-v3]}" == 'one' ]] && echo 'Pass' || echo 'Fail'



# Loop over the output of a command
while read -r mount_point ; do
	echo "Mount Point: ${mount_point}"
done < <( lsblk  --list --noheadings --output name,mountpoint | grep -o '/.*' )


# function toOSPath
## If cygwin, then do something special.
## Otherwise, do nothing

case "$(uname -sr)" in
	CYGWIN*|MINGW*|MINGW32*|MSYS*)
		toOSPath() {
			if test -n "${1:-}"; then
				# positional argument $1";
				cygpath -m "$1" ;
			elif test ! -t 0; then
				# stdin
				cygpath -m -f - ;
			else
				echo "No arguments or standard input." > /dev/stderr
				return 255
			fi
		}
		;;
	*)
		toOSPath() {
			if test -n "${1:-}"; then
				# positional argument $1";
				printf '%s' "$1" ;
			elif test ! -t 0; then
				# stdin
				cat ;
			else
				echo "No arguments or standard input." > /dev/stderr
				return 255
			fi
		}
esac
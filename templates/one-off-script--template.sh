#!/usr/bin/env bash
###################################################################
#
#    A template.
#
#    Parameters
#        1    VAGRANT_DIR
#        2    BOX_FILE_NAME  Convention for Artifactory is ${BOX_NAME}.x86_64_virtualbox$(date +%s).box
#
###################################################################

set -e -E -u -o pipefail

# Bash Version check
#if [[ ${BASH_VERSINFO[0]} -lt 4 ]]; then
#	echo "This script requires Bash version >= 4"
#	exit 1
#fi

# Uncomment to see commands that fail.
trap 'echo "Error at ${BASH_SOURCE[0]}:$LINENO. Command returned $? : $BASH_COMMAND" ; exit 255' ERR

# shellcheck disable=SC2221,SC2222
case "$(uname -sr)" in
    Darwin*)
        # Mac OS X
        #For OSX, use coreutils if installed
        PATH="/usr/local/opt/coreutils/libexec/gnubin:$PATH"
        ;;

    CYGWIN*|MINGW*|MINGW32*|MSYS*)
        # CYGWIN on Windows
        ;;

    # Future Use
    Linux*Microsoft*)
        ;&
    Linux*)
        ;&
    *)
        # Other
        echo "OS $(uname -sr) is not supported by this script"
        exit 255
        ;;
esac

# Get Directory that the script lives in
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

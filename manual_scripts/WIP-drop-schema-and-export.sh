#!/usr/bin/env bash

###################################################################
#
#    WIP script that drops the specifed schemas from the Orcl_Web_Prod VM, preps for export, then exports.

#
#    Parameters
#        None
#
#    Variables expected to be set:
#        None
#
###################################################################

set -e -E -u -o pipefail

# Get Directory that the script lives in
#SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

portal_name="$1"
case "$portal_name" in
    71_schemas)
        vm_name_suffix='71_Schemas'
        drop_schemas=('INFONET73:INFONET73' 'EWP73:EWP73' 'MED:MED')
        ;;
    73_schemas)
        vm_name_suffix='73_Schemas'
        drop_schemas=('INFONET71:INFONET71')
        ;;
    infonet_schemas)
        vm_name_suffix='Infonet_Schemas'
        drop_schemas=('EWP73:EWP73' 'MED:MED')
        ;;
    *)
        vm_name_suffix='All_Schemas'
        drop_schemas=( )
        ;;
esac


# Variables set via Workflow
APPDEV_ORCL_WEB_PROD_DATE='20220213'  # Production Export Date String. Must be safe for filenames.
VBOX_EXPORT_PATH='/Volumes/Scratch_Space_500'
ORACLE_VM_VAGRANT_BASE_BOX_NAME='oracle19c' # Oracle Vagrant Box
ORACLE_VM_VAGRANT_BOX_NAME="${ORACLE_VM_VAGRANT_BASE_BOX_NAME}-${APPDEV_ORCL_WEB_PROD_DATE}"
ORACLE_VM_VBOX_NAME="AppDev_Orcl_Web_Prod_v${APPDEV_ORCL_WEB_PROD_DATE}" # Base Name of Exported Oracle VM
ORACLE_ORCL_SYSTEM_PASS=''
ORACLE_ORCL_SID='ORCL'
ORACLE_ORCL_USER_USER='oracle'

SCRIPTS_DIR='/Users/asteiner/Dev/data/Ec4.x-g2.5/local-env-update-scripts/scripts'
STATE_FOLDER='/Users/asteiner/Dev/data/Ec4.x-g2.5/local-env-update-scripts/states/combined_20220415T044932Z'
# Oracle19c Vagrant Config File
ssh_config_file="${STATE_FOLDER}"/vagrant-ssh-"${ORACLE_VM_VAGRANT_BOX_NAME}"

new_vm_name="${ORACLE_VM_VBOX_NAME}_${vm_name_suffix}"

# Stop the Vagrant and make a Snapshot
echo "Stopping $ORACLE_VM_VAGRANT_BOX_NAME and making a snapshot before continuing."
pushd "${STATE_FOLDER}"/vagrant/"${ORACLE_VM_VAGRANT_BOX_NAME}" > /dev/null
    #vagrant halt
    #vagrant snapshot save before-drops
    #vagrant up
    # TEMP
    vagrant snapshot restore before-export
popd


if [[ "${#drop_schemas[@]}" -gt 0 ]]; then
    echo "Dropping Schemas ${drop_schemas[@]} from Oracle."
    bash "$SCRIPTS_DIR"/run-remote-script.sh \
      --ssh-config-file "${ssh_config_file}" \
      --run-as oracle \
      --run-as-with-login-shell \
      -- \
      "$SCRIPTS_DIR"/oracle/drop-schemas.sh \
      --sid "${ORACLE_ORCL_SID}" \
      --system-pass "${ORACLE_ORCL_SYSTEM_PASS}" \
      "${drop_schemas[@]}"
fi

# After this are the exact same commands found in run-export-oracle-vagrant-as-vm.sh
echo 'Getting Oracle Vagrant Box ready for use outside of Vagrant'
bash "$SCRIPTS_DIR"/run-remote-script.sh \
  --ssh-config-file "${ssh_config_file}" \
  -- \
  "${SCRIPTS_DIR}"/enable-vagrant-user-login.sh "${ORACLE_ORCL_USER_USER}"



echo 'Prepping Oracle Vagrant Box for pack...I mean *export*'
bash "$SCRIPTS_DIR"/run-remote-script.sh \
  --ssh-config-file "${ssh_config_file}" \
  --run-as root \
  -- \
  "${SCRIPTS_DIR}"/oracle/oracle19c-provisioning/guest/prep-oracle-vagrant-for-packaging.sh


echo 'Exporting Oracle VM'
bash "${SCRIPTS_DIR}"/export-vm.sh "${ORACLE_VM_VAGRANT_BOX_NAME}" "${new_vm_name}" "${VBOX_EXPORT_PATH}/${new_vm_name}.ova"

WHENEVER SQLERROR EXIT 3;

SET SERVEROUTPUT ON;

--- Set passwords to a known value...
PROMPT Local-Specific.sql > Changing all passwords
UPDATE USER_
SET
    PASSWORD_ = 'AAAAoAAB9AByppDXfthzQxX32UgUnjTeQXPwd7LoRmGtOqeJ',
    PASSWORDENCRYPTED = 1,
    PASSWORDRESET = 0,
    DIGEST = ''
WHERE
    PASSWORD_ != 'AAAAoAAB9AByppDXfthzQxX32UgUnjTeQXPwd7LoRmGtOqeJ'
    OR
    PASSWORDENCRYPTED != 1
    OR
    PASSWORDRESET != 0
    OR
    DIGEST != ''
;

PROMPT Local-Specific.sql > Changing password policy to not change on login.

--- Don't ask to change password on login
UPDATE PASSWORDPOLICY SET CHANGEREQUIRED = 0 WHERE CHANGEREQUIRED != 0;


PROMPT Local-Specific.sql > Changing Mail Exchange domain.
UPDATE COMPANY SET MX = 'liferay.com' WHERE MX != 'liferay.com';


-- PROMPT Local-Specific.sql > Removing SMTP Host Name configurations.
-- -- Remove all portal preferences of admin.mail.host.names and mail.session.mail.smtp.host
-- UPDATE PORTALPREFERENCES
-- SET
--     PREFERENCES = XMLTYPE(PREFERENCES).transform(
--         XMLTYPE.createxml('
--             <xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
--                 <xsl:output method="xml" omit-xml-declaration="yes"/>
--                 <xsl:template match="/portlet-preferences">
--             <portlet-preferences>
--                     <xsl:for-each select="preference">
--                         <xsl:if test="not(
--                             name/text()=''admin.mail.host.names''
--                             or name/text()=''mail.session.mail.smtp.host''
--                         )">
--                 <preference><name><xsl:value-of select="name/text()"/></name><value><xsl:value-of select="value/text()"/></value></preference>
--                         </xsl:if>
--                     </xsl:for-each>
--             </portlet-preferences>
--                 </xsl:template>
--             </xsl:stylesheet>
--         ')
--     ).getClobVal()
-- WHERE
--     ownerType = 1
-- ;
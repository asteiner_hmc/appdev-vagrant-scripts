WHENEVER SQLERROR EXIT 3;

UPDATE Address
    SET
    street1 = '500 University Drive',
    street2 = '',
    street3 = '',
    city = 'Hershey',
    zip = '17033',
    regionId = (SELECT regionId FROM Region WHERE name = 'Pennsylvania'),
    countryId = (SELECT countryId FROM Country WHERE a3 = 'USA')
;